[![Build Status](https://api.shippable.com/projects/5693988e1895ca447467fc9a/badge/master)](https://api.shippable.com/projects/5693988e1895ca447467fc9a/badge/master)

# Qwirkle

This game is a project for the module 1B for the study Computer Engineering at the University of Twente.

# Rules

The rules can be found on this website:
[rules](http://iloveboardgames.com/wp-content/uploads/2012/02/Qwirkle-Rules.pdf)

# Instructions
- Extract zip anywhere you want, and name it Qwirkle
- Open a commandprompt (Win + R, and type cmd)
- Type `cd Qwirkle`
- To run a server
    - Run `java StartServer`
- To run a client
    - Run `java StartClient`
- Follow instructions, and have fun!

# How to play
Once you enter a server, you will notice that you dont have any tiles yet. Once the player that runs the server hits the  'start server' button, the tiles will be distributed. The control frame will show who's turn it is.
If it is your turn, you can click on a tile on the board. A new window with your hand will appear.
Once you click a tile on this window, you will place the tile on your board.
If you have placed all your tiles, and clicked on 'confirm move', your move will be send to the server, and you will receive points.
The game will end automaticly when the game is over.

# Tiles
All tiles are represented with two characters; a color and a shape.
Once you start the game, you will notice that you only have '--' tiles. This is a 'none-tile', indicating that the server hasn't started yet. Once the server starts the game, you will receive tiles.
The first character is the color (see below), and the second character is the shape (see below)

# Tile representation
| Color | Color Character (always uppercase)  | Shape|  Shape Character (always lowercase)   |
| ------------- |---------------| -----|------|
|Red|R|circle|c|
|Orange|O|diamond|d|
|Blue|B|square|s|
|Yellow|Y|clover|c|
|Green|G|cross|x|
|Purle|P|star|*|