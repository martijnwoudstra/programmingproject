package qwirkle.ai;

import qwirkle.Board;
import qwirkle.exception.InvalidMoveException;
import qwirkle.move.PlaceMove;
import qwirkle.move.SwitchMove;
import qwirkle.move.Turn;
import qwirkle.tiles.Tile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matthijs on 20-1-16.
 */
public class Naive implements Ai {

	Board board;
	ArrayList<Tile> hand;
	Turn.Move move;

	public Naive(Board board, ArrayList<Tile> hand) {
		this.board = board;
		this.hand = hand;
	}

	public Turn.Move determineMove(boolean hint) {
		boolean isSet = false;
		List<Integer[]> emptyFields = board.emptyFields();
		int i = 0;
		for (Tile t : hand) {
			for (Integer[] coord : emptyFields) {
				if (!isSet) {
					if (board.isValidMove(t, coord[0], coord[1])) {
						PlaceMove pm = new PlaceMove(board);
						try {
							pm.addPlaceMove(t, coord[0], coord[1]);
							if(!hint){
								hand.remove(i);
							}
							isSet = true;
							move = pm;
							return pm;
						} catch (InvalidMoveException e) {
							System.err.println(e);
						}
					}
				}

			}
			i++;
		}
		// do swap
		int random = (int) (Math.random() * hand.size());
		Tile t = hand.get(random);
		while (t.equalsTile(Board.NONE_TILE)) {
			random = (int) (Math.random() * hand.size());
			t = hand.get(random);
		}
		SwitchMove sw = new SwitchMove(board);
		sw.addSwitchMove(t);
		move = sw;
		return sw;
	}

	public Turn.Move getMove(){
		return move;
	}

}
