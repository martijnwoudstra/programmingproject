package qwirkle.ai;

import qwirkle.move.Turn;

/**
 * Created by matthijs on 20-1-16.
 */
public interface Ai {

    Turn.Move determineMove(boolean hint);
    Turn.Move getMove();
}
