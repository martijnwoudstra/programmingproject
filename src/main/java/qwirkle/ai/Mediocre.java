package qwirkle.ai;

import qwirkle.Board;
import qwirkle.exception.InvalidMoveException;
import qwirkle.move.PlaceMove;
import qwirkle.move.SwitchMove;
import qwirkle.move.Turn;
import qwirkle.tiles.Tile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by martijn on 27-1-16.
 */
public class Mediocre implements Ai {

    Board board;
    ArrayList<Tile> hand;
    Turn.Move move;

    public Mediocre(Board board, ArrayList<Tile> hand) {
        this.board = board;
        this.hand = hand;
    }

    public Turn.Move determineMove(boolean hint) {
        if(board.isEmpty(91, 91)){
            move = getBestHand();
            return move;
        }
        boolean isSet = false;
        List<Integer[]> emptyFields = board.emptyFields();
        int i = 0;
        PlaceMove theMove = new PlaceMove(board);
        int highestScore = 0;
        Tile highestTile = null;

        for (Tile t : hand) {
            for (Integer[] coord : emptyFields) {
                    if (board.isValidMove(t, coord[0], coord[1])) {
                        PlaceMove pm = new PlaceMove(board);
                        try {
                            pm.addPlaceMove(t, coord[0], coord[1]);
                            int score = pm.getScore();
                            if(highestScore < score){
                                highestScore = score;
                                theMove = pm;
                                highestTile = t;
                            }
                        } catch (InvalidMoveException e) {
                            e.printStackTrace();
                        }
                    }
            }
            i++;
        }
        if(highestScore > 0) {
            move = theMove;
            hand.remove(highestTile);
            return theMove;
        }
        // do swap
        int random = (int) (Math.random() * hand.size());
        Tile t = hand.get(random);
        while (t.equalsTile(Board.NONE_TILE)) {
            random = (int) (Math.random() * hand.size());
            t = hand.get(random);
        }
        SwitchMove sw = new SwitchMove(board);
        sw.addSwitchMove(t);
        move = sw;
        return sw;
    }

    public Turn.Move getMove(){
        return move;
    }


    public PlaceMove getBestHand() {
        ArrayList<Tile> hand = board.getGame().getLocalPlayer().getHand();
        Set<Tile> bestLocal = new HashSet<Tile>();
        // Set for colors
        ArrayList<Set<Tile>> colors = new ArrayList<Set<Tile>>();
        ArrayList<Set<String>> colorsString = new ArrayList<Set<String>>();
        for (int i = 0; i < 6; i++) {
            colors.add(new HashSet<Tile>());
            colorsString.add(new HashSet<String>());
        }

        // Set for colors
        // Set for colors
        ArrayList<Set<Tile>> shapes = new ArrayList<Set<Tile>>();
        ArrayList<Set<String>> shapesString = new ArrayList<Set<String>>();
        for (int i = 0; i < 6; i++) {
            shapes.add(new HashSet<Tile>());
            shapesString.add(new HashSet<String>());
        }

        for (Tile t : hand) {
            switch (t.getColor()) {
                case RED:
                    if (!colorsString.get(0).contains(t.toString())) {
                        colors.get(0).add(t);
                        colorsString.get(0).add(t.toString());
                    }
                    break;
                case ORANGE:
                    if (!colorsString.get(1).contains(t.toString())) {
                        colors.get(1).add(t);
                        colorsString.get(1).add(t.toString());
                    }
                    break;
                case BLUE:
                    if (!colorsString.get(2).contains(t.toString())) {
                        colors.get(2).add(t);
                        colorsString.get(2).add(t.toString());
                    }
                    break;
                case PURPLE:
                    if (!colorsString.get(3).contains(t.toString())) {
                        colors.get(3).add(t);
                        colorsString.get(3).add(t.toString());
                    }
                    break;
                case YELLOW:
                    if (!colorsString.get(4).contains(t.toString())) {
                        colors.get(4).add(t);
                        colorsString.get(4).add(t.toString());
                    }
                    break;
                case GREEN:
                    if (!colorsString.get(5).contains(t.toString())) {
                        colors.get(5).add(t);
                        colorsString.get(5).add(t.toString());
                    }
                    break;
                default:
                    break;
            }

            switch (t.getShape()) {
                case CIRCLE:
                    if (!shapesString.get(0).contains(t.toString())) {
                        shapes.get(0).add(t);
                        shapesString.get(0).add(t.toString());
                    }
                    break;
                case DIAMOND:
                    if (!shapesString.get(1).contains(t.toString())) {
                        shapes.get(1).add(t);
                        shapesString.get(1).add(t.toString());
                    }
                    break;
                case SQUARE:
                    if (!shapesString.get(2).contains(t.toString())) {
                        shapes.get(2).add(t);
                        shapesString.get(2).add(t.toString());
                    }
                    break;
                case CLOVER:
                    if (!shapesString.get(3).contains(t.toString())) {
                        shapes.get(3).add(t);
                        shapesString.get(3).add(t.toString());
                    }
                    break;
                case CROSS:
                    if (!shapesString.get(4).contains(t.toString())) {
                        shapes.get(4).add(t);
                        shapesString.get(4).add(t.toString());
                    }
                    break;
                case STAR:
                    if (!shapesString.get(5).contains(t.toString())) {
                        shapes.get(5).add(t);
                        shapesString.get(5).add(t.toString());
                    }
                    break;
                default:
                    break;
            }
        }
        for (int i = 0; i < colors.size(); i++) {
            if (colors.get(i).size() > bestLocal.size()) {
                bestLocal = colors.get(i);
            }
        }
        // overwrite with biggest shape set if there is a bigger shape set
        for (int i = 0; i < shapes.size(); i++) {
            if (shapes.get(i).size() > bestLocal.size()) {
                bestLocal = shapes.get(i);
            }
        }
        PlaceMove pm = new PlaceMove(board);
        int col = 91;
        for (Tile t : bestLocal) {
            try {
                pm.addPlaceMove(t, 91, col);
                col++;
            } catch (InvalidMoveException e) {
                System.err.println(e);
            }
        }
        return pm;
    }
}
