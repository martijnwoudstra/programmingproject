package qwirkle.tiles;

/**
 * Created by martijn on 18-12-15.
 */
public class Tile {

    private Color color;
    private Shape shape;
    /*
    private static BufferedImage[] sprites;
    static final int width = 10;
    static final int height = 10;
    static final int rows = 6;
    static final int cols = 6;
    static final int startRow = 100;
    static final int startCol = 100;
    */
    /**.
     * Creates a tile object
     *
     * @param color
     *      {@link qwirkle.tiles.Color} object which the tile should have
     * @param shape
     *      {@link qwirkle.tiles.Shape} object which the tile should have
     */
    public Tile(Color color, Shape shape) {
        this.color = color;
        this.shape = shape;
        //sprites = new BufferedImage[rows * cols];
    }

    /**.
     * Checks if this equals the tile argument
     * @param tile
     *      {@link qwirkle.tiles.Tile} object
     * @return Boolean true fi tiles are equal, otherwise false.
     */
    public boolean equalsTile(Tile tile) {
        return this.toString().equals(tile.toString());
    }

    /**.
     * Returns the {@link qwirkle.tiles.Color} of of the {@link qwirkle.tiles.Tile}
     *
     * @return {@link qwirkle.tiles.Color} object
     */
    //@pure
    public Color getColor() {
        return color;
    }

    /**.
     * Returns the {@link qwirkle.tiles.Shape} of the {@link qwirkle.tiles.Tile}
     *
     * @return {@link qwirkle.tiles.Shape} object
     */
    //@pure
    public Shape getShape() {
        return shape;
    }

    @Override
    public String toString() {
        return "" + color.getChar() + shape.getChar();
    }

    /*
    public BufferedImage getIcon(){
        return sprites[(this.color.ordinal() * 6) + this.shape.ordinal()];
    }

    public static void addIcons(String location){

        BufferedImage bigImg = null;
        try {
            bigImg = ImageIO.read(new File(location));
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedImage[] sprites = new BufferedImage[rows * cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                sprites[(i * cols) + j] = bigImg.getSubimage(
                        startCol + j * width,
                        startRow + i * height,
                        width,
                        height
                );
            }
        }
    }
    */
}
