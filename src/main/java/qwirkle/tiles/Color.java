package qwirkle.tiles;

import qwirkle.lib.TileLib;

public enum Color {
    /**
     * All Colors in the game. Uses chars from TileLib.
     */
    RED(TileLib.RED_CHAR), ORANGE(TileLib.ORANGE_CHAR), 
    YELLOW(TileLib.YELLOW_CHAR), GREEN(TileLib.GREEN_CHAR),  
    BLUE(TileLib.BLUE_CHAR), PURPLE(TileLib.PURPLE_CHAR),
    NONE(TileLib.NONE_CHAR);

    private char id;

    /**.
     * Sets the identifier for a Color
     *
     * @param identifier
     *         Char identifier of the Color
     */
    Color(char identifier) {
        id = identifier;
    }

    /**.
     * Returns all colors, except Color.NONE
     *
     * @return Color[] of all possible Colors.
     */
    public static Color[] getColors() {
        return new Color[]{Color.RED, Color.ORANGE, Color.BLUE, 
        		           Color.PURPLE, Color.YELLOW, Color.GREEN};
    }

    /**.
     * Returns the Char that belongs to the current Color object
     *
     * @return Char identifier
     */
    public char getChar() { //TODO discuss types of enum during protocol meeting
        return id;
    }

    /**.
     * Returns a {@link qwirkle.tiles.Color} belonging 
     * to the corresponding char defined in {@link TileLib}
     * @param character
     *      Char of the {@link qwirkle.tiles.Color}
     * @return {@link qwirkle.tiles.Color} object
     */
    public static Color generateColorFromChar(char character) {
        switch (character) {
            case TileLib.RED_CHAR:
                return RED;
            case TileLib.ORANGE_CHAR:
                return ORANGE;
            case TileLib.BLUE_CHAR:
                return BLUE;
            case TileLib.PURPLE_CHAR:
                return PURPLE;
            case TileLib.YELLOW_CHAR:
                return YELLOW;
            case TileLib.GREEN_CHAR:
                return GREEN;
            default:
                return NONE;
        }
    }

    @Override
    public String toString() {
        return "" + id;
    }
}
