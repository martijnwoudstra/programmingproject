package qwirkle.tiles;

import qwirkle.lib.TileLib;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum Shape {
	/**
	 * All possible shapes in the game. Uses the chars defined in TileLib
	 */
	CIRCLE(TileLib.CIRCLE_CHAR), CROSS(TileLib.CROSS_CHAR), DIAMOND(TileLib.DIAMOND_CHAR), SQUARE(
			TileLib.SQUARE_CHAR), STAR(TileLib.STAR_CHAR), 
			CLOVER(TileLib.CLOVER_CHAR), NONE(TileLib.NONE_CHAR);

	public static final List<Shape> SHAPE_LIST = 
			  Collections.unmodifiableList(Arrays.asList(values()));

	private char id;

	/**
	 * . Sets the identifier for a Shape
	 *
	 * @param identifier
	 *            Char identifier of the Shape.
	 */
	Shape(char identifier) {
		id = identifier;
	}

	/**
	 * . Returns all shapes, except Shape.None
	 *
	 * @return Shape[] of all possible Shapes.
	 */
	public static Shape[] getShapes() {
		return new Shape[] {Shape.CIRCLE, Shape.DIAMOND, 
			Shape.SQUARE, Shape.CLOVER, Shape.CROSS, Shape.STAR };
	}

	/**.
	 * Returns the Char that belongs to the current Shape object
	 *
	 * @return Char identifier
	 */
	public char getChar() { 
		return id;
	}

	/**.
	 * Returns a {@link qwirkle.tiles.Shape} belonging to the corresponding char
	 * defined in {@link TileLib}
	 * 
	 * @param character
	 *            Char of the {@link qwirkle.tiles.Shape}
	 * @return {@link qwirkle.tiles.Shape} object
	 */
	public static Shape generateShapeFromChar(char character) {
		switch (character) {
			case TileLib.CIRCLE_CHAR:
				return CIRCLE;
			case TileLib.DIAMOND_CHAR:
				return DIAMOND;
			case TileLib.SQUARE_CHAR:
				return SQUARE;
			case TileLib.CLOVER_CHAR:
				return CLOVER;
			case TileLib.STAR_CHAR:
				return STAR;
			case TileLib.CROSS_CHAR:
				return CROSS;
			default:
				return NONE;
		}
	}

	@Override
	public String toString() {
		return "" + id;
	}
}
