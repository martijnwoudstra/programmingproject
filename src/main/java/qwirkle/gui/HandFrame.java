package qwirkle.gui;

import qwirkle.exception.InvalidMoveException;
import qwirkle.tiles.Tile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class HandFrame extends JFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * Construeert een frame waarin de hand van de speler word weergegeven
     * TO DO: passing type to actionperformed
     * TO DO: Passing hand
     * TO DO: More info in actionperformed
     * TO DISCUSS: Should it be possible to undo valid action? ANS: NO
     */
    int row;
    int col;
    GUIView view;
    JLabel error;

    /**.
     * Create the frame on which you can chose which tile you want to place
     * @param row The y-coordinate where you want to place the tile
     * @param col The x-coordinate where you want to place the tile
     * @param view the view controller
     */
    public HandFrame(int row, int col, GUIView view) {
        super(String.format("Select tile for (%d,%d)", row, col));
        this.row = row;
        this.col = col;
        this.view = view;

        Container panel = getContentPane();
        panel.setLayout(new BorderLayout());

        error = new JLabel();
        error.setText("Test");
        panel.add(BorderLayout.NORTH, error);


        JPanel handPanel = new JPanel();
        handPanel.setLayout(new FlowLayout());
        JButton button;
        ArrayList<Tile> hand = view.getGame().getLocalPlayer().getHand();
        for (int i = 0; i < view.getGame().getLocalPlayer().getHand().size(); i++) {

            button = new JButton(hand.get(i).toString());
            // action listener on button
            button.addActionListener(new SelectTileButtonListener(row, col, hand.get(i)));
            handPanel.add(button);


        }
        panel.add(BorderLayout.SOUTH, handPanel);


        setSize(500, 100);
    }


    class SelectTileButtonListener implements ActionListener {
        private Tile tile;
        private int row;
        private int col;

        public SelectTileButtonListener(int row, int col, Tile t) {
            this.tile = t;
            this.row = row;
            this.col = col;
        }

        public void actionPerformed(ActionEvent e) {
//            Board deepCopy = view.getDeepCopy();
//            boolean isValid = deepCopy.placeTile(tile, row, col);
//            if(isValid){
//                // change button to the tile
//                view.setTile(row, col, "type");
//
//            }
            System.out.println("Gui: Set Tile");
            try {
                view.getGame().getBoard().addTile(tile, row, col);
                System.out.println("Gui: Valid move");
                //change button to the tile
                view.setTile(row, col, tile.toString());
                // disable swap function
                view.getControlFrame().toggleSwap(false);
                // close hand frame
                setVisible(false);
                dispose();
            } catch (InvalidMoveException e1) {
                System.out.println("Gui: Invalid move");
                error.setText("This is not a valid move!");
            }
        }
    }

}
