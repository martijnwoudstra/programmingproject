package qwirkle.gui;

import qwirkle.exception.InvalidMoveException;
import qwirkle.lib.GameRules;
import qwirkle.move.PlaceMove;
import qwirkle.move.SwitchMove;
import qwirkle.move.Turn;
import qwirkle.player.Player;
import qwirkle.tiles.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

public class ControlFrame extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Construeert een frame waarin de hand van de speler word weergegeven, en
	 * de swapIndexHand en confirm button TO DO: passing type to actionperformed
	 * TO DO: Passing hand TO DO: More info in actionperformed TO DISCUSS:
	 * Should it be possible to undo valid action?
	 */
	GUIView view;
	JLabel error;
	JLabel[] name = new JLabel[GameRules.MAX_PLAYERS];
	JLabel[] score = new JLabel[GameRules.MAX_PLAYERS];
	JButton[] hand = new JButton[GameRules.MAX_HAND_SIZE];
	JButton confirm = new JButton();
	JButton swap = new JButton();
	JButton hint = new JButton();
	ArrayList<Integer> swapIndexHand = new ArrayList<Integer>();

	private static final String CONFIRM = "Confirm set";
	private static final String SWAP = "Swap";
	private static final String HINT = "Hint";

	/**
	 * . Initialise the controlframe where you can see your hand and
	 * swap/confirm your move
	 * 
	 * @param view
	 *            the view controller
	 */
	public ControlFrame(GUIView view) {
		super("Controller");
		this.view = view;

		Container panel = getContentPane();
		panel.setLayout(new BorderLayout());

		// Error message
		error = new JLabel();
		panel.add(BorderLayout.NORTH, error);

		// Hand
		JPanel handPanel = new JPanel();
		handPanel.setLayout(new GridLayout(1, 6));

		for (int i = 0; i < GameRules.MAX_HAND_SIZE; i++) {

			hand[i] = new JButton("");
			String buttonText = i < view.getGame().getLocalPlayer().getHand().size()
				  	? view.getGame().getLocalPlayer().getHand().get(i).toString() : "-";
			hand[i].setText(String.format("%d %s", i, buttonText));
			hand[i].addActionListener(this);
			handPanel.add(hand[i]);
		}

		panel.add(BorderLayout.CENTER, handPanel);

		// Players
		JPanel players = new JPanel();
		players.setLayout(new GridLayout(GameRules.MAX_PLAYERS, 2));
		for (int i = 0; i < GameRules.MAX_PLAYERS; i++) {
			name[i] = new JLabel();

			score[i] = new JLabel();

			players.add(name[i]);
			players.add(score[i]);

		}

		panel.add(BorderLayout.EAST, players);

		// Footer
		JPanel footer = new JPanel();

		// Create swapIndexHand and confirm button
		confirm = new JButton(CONFIRM);
		confirm.addActionListener(this);
		swap = new JButton(SWAP);
		swap.addActionListener(this);
		hint = new JButton(HINT);
		hint.addActionListener(this);
		footer.add(confirm);
		footer.add(swap);
		footer.add(hint);

		panel.add(BorderLayout.SOUTH, footer);

		setSize(600, 200);
		// unable to close
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * . Show a error message on the view
	 * 
	 * @param text
	 *            String text that should be displayed
	 */
	public void error(String text) {
		error.setText("Error: " + text);
	}

	/**
	 * . Show a message on the view
	 * 
	 * @param text
	 *            String text that should be displayed
	 */
	public void message(String text) {
		error.setText(text);
	}

	/**
	 * . render the players
	 */
	public void renderPlayer() {
		HashMap<Integer, Player> players = (HashMap<Integer, Player>) view.getGame().getPlayers();
		for (int i = 0; i < name.length; i++) {
			name[i].setText("");
			score[i].setText("");
		}
		for (int key : players.keySet()) {
			name[key].setText(players.get(key).name);
			score[key].setText(""+players.get(key).getScore());
		}
	}

	/**
	 * . Update the score of the player on the view
	 *
	 * @param playerNumber
	 *            The number of the player
	 */
	public void updateScore(int playerNumber) {
		score[playerNumber].setText("" + view.getGame().getPlayer(playerNumber).getScore());
	}

	/**
	 * . Handle a kicked user
	 * 
	 * @param playerNumber
	 *            The number of the player that is kicked
	 * @param reason
	 *            the reason why the player is kicked
	 */
	public void kick(int playerNumber, String reason) {
		error(String.format("Player %d has been kicked for reason: %s", playerNumber, reason));
		renderPlayer();

	}

	/**
	 * . Actionlistener to listen to all the buttons on the controlframe
	 */
	public void actionPerformed(ActionEvent e) {
		// TODO inform server these moves are confirmed
		// TODO Disable confirmed tiles
		// TODO Implement SWAP
		JButton button = (JButton) e.getSource();
		switch (button.getText()) {
			case CONFIRM:
				confirmMove();
				break;
			case SWAP:
				if (swapIndexHand.size() > 0) {
					swap();
				}
				break;
			case HINT:
				Turn.Move theHint = view.getGame().getBoard().hint();
				if (theHint instanceof PlaceMove) {
					PlaceMove phint = (PlaceMove) theHint;
					List<Integer> coords = phint.getMoves().get(0).getCoordinates();
					Tile t = phint.getMoves().get(0).getTile();
					message(String.format("You can place an %s on (%d, %d)", 
							  t.toString(), coords.get(0), coords.get(1)));
				}
				break;
			// add tile to swapIndexHand tiles
			default:
				int index = 0;
				try {
					index = Integer.parseInt("" + button.getText().charAt(0));
				} catch (NumberFormatException ed) {
					System.out.println(ed.getMessage());
				}
				if (swapIndexHand.contains(index)) {
					swapIndexHand.remove((Object) index);
					button.setBackground(null);
				} else {
					swapIndexHand.add(index);
					button.setBackground(Color.red);
				}

		}
	}

	/**.
	 * Update the hand on the controlframe
	 */
	public void updateHand() {
		ArrayList<Tile> theHand = view.getGame().getLocalPlayer().getHand();
		for (int i = 0; i < GameRules.MAX_HAND_SIZE; i++) {
			String t;
			if (i >= theHand.size()) {
				t = "-";
			} else {
				t = theHand.get(i).toString();
			}
			this.hand[i].setText(String.format("%d %s", i, t));
		}
	}

	/**.
	 * Toggle the swap button
	 * 
	 * @param toggle
	 *            Boolean to which state the button should toggle
	 */
	public void toggleSwap(boolean toggle) {
		if (view.getGame().getBoard().getStackSize() == 0) {
			swap.setEnabled(false);
		} else {
			swap.setEnabled(toggle);
		}

	}

	/**.
	 * Toggle the confirm button
	 * 
	 * @param toggle
	 *            Boolean to which state the button should toggle
	 */
	public void toggleConfirm(boolean toggle) {
		confirm.setEnabled(toggle);
	}

	/**.
	 * Toggle the swap and confirm button both
	 * 
	 * @param toggle
	 *            Boolean to which state the buttons should toggle
	 */
	public void toggleButtons(boolean toggle) {
		toggleConfirm(toggle);
		toggleSwap(toggle);

	}

	/**.
	 * Confirm the move which the player has done
	 */
	private void confirmMove() {
		System.out.println("Gui: try to confirm");
		boolean validMove = view.getGame().getBoard().confirmMove();
		if (validMove) {
			System.out.println("Gui: confirm -> valid move");
			message("Set confirmed");
			resetHandBackground();
		} else {
			System.out.println("Gui: confirm -> invalid move");
		}
	}

	/**.
	 * Confirm the swap move of the player
	 */
	private void swap() {
		System.out.println("Gui: do Swap");
		Tile[] swapTiles = new Tile[swapIndexHand.size()];
		SwitchMove move = new SwitchMove(view.getGame().getBoard());
		for (int i = 0; i < swapIndexHand.size(); i++) {
			Tile t = view.getGame().getLocalPlayer().getHand().get(swapIndexHand.get(i));
			swapTiles[i] = t;
			move.addSwitchMove(t);
		}
		try {
			move.confirmMyMove();
			// reset layout of all buttons
			resetHandBackground();
			swapIndexHand = new ArrayList<Integer>();
		} catch (InvalidMoveException e) {
			System.out.println("Gui: error " + e.getMessage());
			error(e.getMessage());
		}
	}

	/**.
	 * Set the backgrounds of the hand to the initial background
	 */
	private void resetHandBackground() {
		for (int i = 0; i < GameRules.MAX_HAND_SIZE; i++) {
			hand[i].setBackground(null);
		}
	}
}
