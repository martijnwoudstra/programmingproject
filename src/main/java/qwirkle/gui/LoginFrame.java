package qwirkle.gui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

public class LoginFrame extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Grafische componenten
	private GUIView view;
    private JTextField hostField;
    private JTextField portField;
	private JTextField nameField;
	private JCheckBox aiField;
    private JLabel errorLabel;


	/**.
	 * Create a frame to connect to the server
	 * @param view the view controller
     */
	public LoginFrame(GUIView view) {
		super("Connect to server");
        this.view = view;

		Container panel = getContentPane();
		panel.setLayout(new GridLayout(6, 1));

		JPanel hostPanel = new JPanel();
		JPanel portPanel = new JPanel();
		JPanel namePanel = new JPanel();
		JPanel aiPanel = new JPanel();

		// Create labels
		JLabel hostLabel = new JLabel("host:");
		JLabel portLabel = new JLabel("port:");
		JLabel nameLabel = new JLabel("name:");
		JLabel aiLabel = new JLabel("AI:");
		hostPanel.add(hostLabel);
		portPanel.add(portLabel);
		namePanel.add(nameLabel);
		aiPanel.add(aiLabel);

		// Create textfields
		hostField = new JTextField(10);
		hostField.setText("localhost");
		portField = new JTextField(10);
        portField.setText("8745");
		nameField = new JTextField(10);
        nameField.setText("test");
		aiField = new JCheckBox();
		hostPanel.add(SpringLayout.EAST, hostField);
		portPanel.add(SpringLayout.EAST, portField);
		namePanel.add(SpringLayout.EAST, nameField);
		aiPanel.add(aiField);

		// Create connect button
		JButton connectButton = new JButton("Connect");

		// action listener on connectButton
		connectButton.addActionListener(this);

		// Error label
		errorLabel = new JLabel(" ");


		// Link to panel
		panel.add(hostPanel);
		panel.add(portPanel);
		panel.add(namePanel);
		panel.add(aiPanel);
		panel.add(errorLabel);
		panel.add(connectButton);

		setSize(550, 160);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (!hostField.getText().equals("") && !portField.getText().equals("")) {
            // TO DO: Check port is number
            try {
                view.getGame().connectToServer(hostField.getText(), 
                		  Integer.parseInt(portField.getText()), 
                		  nameField.getText(), aiField.isSelected());
            } catch (NumberFormatException e) {
                errorLabel.setText("Port must be numberic");
            } catch (IOException e) {
                errorLabel.setText(e.getMessage());
            }


        } else {
            errorLabel.setText("Host and port must be filled in");
        }
    }
}
