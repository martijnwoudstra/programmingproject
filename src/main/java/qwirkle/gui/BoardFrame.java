package qwirkle.gui;

import qwirkle.lib.GameRules;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BoardFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**.
	 * Construeert een board waarop alle tiles te zien zijn en geplaatst kunnen
	 * worden
	 */

	private JButton[][] button = new JButton[GameRules.BOARD_ROWS][GameRules.BOARD_COLS];
	private GUIView view;
	private JPanel panel;

	/*
	 * Declare default screen values This is what you see on the screen, so left
	 * top is minRow,minCol and right bottom is maxRow, maxCol
	 */
	// @ invariant minRow <= maxRow && minCol <= maxCol;
	// private int minRow = 85;
	// private int maxRow = 100;
	// private int minCol = 85;
	// private int maxCol = 100;
	//
	// private int rows = maxRow - minRow;
	// private int cols = maxCol - minCol;

	/**.
	 * Initial the boardFrame
	 * 
	 * @param view
	 *            the view controller
	 */
	public BoardFrame(GUIView view) {
		// Initialisatie grafische componenten
		super("Qwirkle");
		this.view = view;

		// Board
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		// button = new JButton[maxRow + 1][maxCol + 1];
		// for (int row = minRow; row <= maxRow; row++) {
		// int i = 1;
		// for (int col = minCol; col <= maxCol; col++){
		// button[row][col] = new JButton("");
		// if(row == 91 && col == 91){
		// button[row][col].setBackground(Color.red);
		// }
		// //button[row][col].setText("(" + row + "," + col + ")");
		// // action listener on button
		// button[row][col].addActionListener(new PlaceTileButtonListener(row, col));
		// c.gridx = row;
		// c.gridy = col;
		// panel.add(button[row][col], c);
		// }
		// }

		makeButton(91, 91);
		button[91][91].setBackground(Color.red);
		// Scroll panel
		JScrollPane scroll = new JScrollPane(panel);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		Container cc = getContentPane();
		cc.add(scroll);

		setSize(500, 500);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**.
	 * Place a tile on the board
	 * 
	 * @param row
	 *            int row where the tile should be placed
	 * @param col
	 *            int col where the tile should be placed
	 * @param type
	 *            String type the type that the tile should be
	 */
	public void setTile(int row, int col, String type) {
		button[row][col].setText(type);
		generateNewButtons(row, col);
	}

	/**.
	 * Check if you should make a new button on the view
	 * 
	 * @param row
	 *            the row around where you should check
	 * @param col
	 *            the col around where you should check
	 */
	private void generateNewButtons(int row, int col) {
		// check down
		if (button[row + 1][col] == null) {
			makeButton(row + 1, col);
		}
		// check up
		if (button[row - 1][col] == null) {
			makeButton(row - 1, col);
		}
		// check right
		if (button[row][col + 1] == null) {
			makeButton(row, col + 1);
		}
		// check left
		if (button[row][col - 1] == null) {
			makeButton(row, col - 1);
		}

	}

	/**.
	 * Create a new button on the board view
	 * 
	 * @param row
	 *            The y-coordinate where the button should be created
	 * @param col
	 *            The x-coordinate where the button should be created
	 */
	private void makeButton(int row, int col) {
		GridBagConstraints c = new GridBagConstraints();
		button[row][col] = new JButton("");
		Dimension d = new Dimension(60, 60);
		button[row][col].setPreferredSize(d);
		button[row][col].setMinimumSize(d);
		button[row][col].setMaximumSize(d);
		c.gridx = col;
		c.gridy = row;
		panel.add(button[row][col], c);
		button[row][col].addActionListener(new PlaceTileButtonListener(row, col));
		panel.revalidate();
		panel.repaint();
	}

	/**.
	 * Disable the tile
	 * 
	 * @param row
	 *            int row where the tile should be disabled
	 * @param col
	 *            int col where the tile should be disabled
	 */
	public void disableTile(int row, int col) {
		button[row][col].setEnabled(false);
	}

	class PlaceTileButtonListener implements ActionListener {
		private int row;
		private int col;

		/**.
		 * Initialise the action listeren to place a tile
		 * 
		 * @param row
		 *            The row where a tile should be placed
		 * @param col
		 *            The col where a tile should be placed
		 */
		public PlaceTileButtonListener(int row, int col) {
			this.row = row;
			this.col = col;
		}

		/**.
		 * actionlistener to listen to the buttons on the board frame
		 * 
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if (view.getGame().getCurrentPlayer() == view.getGame().getLocalPlayer()) {
				HandFrame hand = new HandFrame(row, col, view);
				hand.setVisible(true);
			}
		}
	}
}
