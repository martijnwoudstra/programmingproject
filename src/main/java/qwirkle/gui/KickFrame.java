package qwirkle.gui;


import javax.swing.*;
import java.awt.*;

public class KickFrame extends JFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**.
     * Initialise an frame to inform that the player is kicked
     */

    public KickFrame(String reason) {
        super("You have been kicked");

        Container panel = getContentPane();
        panel.setLayout(new FlowLayout());

        JLabel label = new JLabel("You have been kicked for reason: " + reason);
        panel.add(label);

        setSize(500, 100);
        // unable to close
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

}
