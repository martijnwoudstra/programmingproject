package qwirkle.gui;

import qwirkle.Game;
import qwirkle.lib.Protocol;
import qwirkle.network.ClientConnector;
import qwirkle.player.Player;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;


public class GUIView implements Observer {
    private Game game;
    private LoginFrame login;
    private BoardFrame boardFrame;
    private ControlFrame controlFrame;
    public static final String UPDATE = "UPDATE";
    public static final String NOCONNECTION = "NOCONNECTION";

    public GUIView(Game game) {
        this.game = game;
        this.start();
    }

    public void start() {
        login = new LoginFrame(this);
        login.setVisible(true);
    }

    /**.
     * Methode to show the board and the controler after login
     */
    public void showGame() {
        boardFrame = new BoardFrame(this);
        boardFrame.setVisible(true);

        controlFrame = new ControlFrame(this);
        controlFrame.setVisible(true);
    }

    /**.
     * Get the game object
     * @return returns the game object
     */
    public Game getGame() {
        return game;
    }

    /**.
     * Get the controlFrame object
     * @return returns the ControlFrame
     */
    public ControlFrame getControlFrame() {
        return controlFrame;
    }

    /**.
     * Set a tile on the board an disable the button
     * @param row The row where it should be placed
     * @param col The col where it should be placed
     * @param type The text representation of the tile
     */
    public void setTile(int row, int col, String type) {
        boardFrame.setTile(row, col, type);
        boardFrame.disableTile(row, col);
    }

    public void reset(){
        closeGUI();
        game.reset();
        start();
    }

    @Override
    /**
     * Handle the data between backend and gui
     */
    public void update(Observable observable, Object arg) {
        System.out.println("Back->Gui Command: " + arg);
        if (observable instanceof ClientConnector) {
            if (arg instanceof String) {
                Scanner scan = new Scanner(arg.toString());
                String keyword = scan.next();
                // Login accepted
                switch (keyword) {
                    case Protocol.Server.LOGIN_ACCEPTED:
                        System.out.println("Gui: Login accepted");
                        showGame();
                        login.setVisible(false);
                        login.dispose();
                        break;
                    // New turn
                    case Protocol.Server.TURN:
                        System.out.println("Gui: Turn done by a user");
                        // Skip playernumber
                        scan.next();
                        String tile;
                        int row;
                        int col;
                        
                        while (scan.hasNext() && (tile = scan.next()) != null 
                        		&& scan.hasNextInt()) {
                            row = scan.nextInt();
                            if (scan.hasNextInt()) {
                                col = scan.nextInt();
                                setTile(row, col, tile);
                            }

                        }
                        break;
                    // Score update
                    case UPDATE: {
                        System.out.println("Gui: Update score");
                        int playerNumber;
                        if (scan.hasNextInt()) {
                            playerNumber = scan.nextInt();
                            if (scan.hasNextInt()) {
                                controlFrame.updateScore(playerNumber);
                            }
                        }
                        break;
                    }
                    // Tile update
                    case Protocol.Server.NEW: {
                        System.out.println("Gui: New tile received");
                        System.out.println(game.getLocalPlayer().getHand());
                        controlFrame.updateHand();
                        break;
                    }
                    // Next user
                    case Protocol.Server.NEXT: {
                        System.out.println("Gui: Next player");
                        if (getGame().getCurrentPlayerIdentifier() == 
                        		  getGame().getOwnPlayerIdentifier()) {
                            controlFrame.message("It's your turn!");
                            controlFrame.toggleButtons(true);
                        } else {
                            int curPlayer = getGame().getCurrentPlayerIdentifier();
                            controlFrame.message("It's the turn of player: " + curPlayer);
                            controlFrame.toggleButtons(false);
                        }
                        break;
                    }
                    // Kick user
                    case Protocol.Server.KICK: {
                        System.out.println("Gui: User kicked");
                        String reason;
                        int playerNumber;
                        if (scan.hasNextInt()) {
                            playerNumber = scan.nextInt();
                            System.out.println(playerNumber);
                            if ((reason = scan.nextLine()) != null) {
                                System.out.println(reason);
                                if (playerNumber == game.getOwnPlayerIdentifier()) {
                                    new KickFrame(reason);
                                    closeGUI();
                                } else {
                                    controlFrame.kick(playerNumber, reason);
                                }
                            }
                        } else {
                            System.out.println(scan.nextInt());
                        }
                        break;
                    }
                    // Winner
                    case Protocol.Server.WINNER:
                        System.out.println("Gui: Winner");
                        if (scan.hasNextInt()) {
                            int playerNumber = scan.nextInt();
                            if (game.getOwnPlayerIdentifier() == playerNumber) {
                                new WinnerFrame(this, true, playerNumber);
                            } else {
                                new WinnerFrame(this, false, playerNumber);
                            }
                            controlFrame.setVisible(false);
                            controlFrame.dispose();

                        }
                        break;
                    // connection lost
                    case NOCONNECTION:
                        controlFrame.error("Connection with server lost!");
                        boardFrame.setVisible(false);
                        boardFrame.dispose();
                        controlFrame.toggleButtons(false);
                        break;
                    default:
                        System.out.println("Gui: Unhandled back->gui communication" + arg);
                }
                scan.close();
            } else if (arg instanceof Player) {
                // new User
                System.out.println("Gui: New user received");
                controlFrame.renderPlayer();
            }  
        }
    }

    /**.
     * Close the board and control frame
     */
    public void closeGUI() {
        boardFrame.setVisible(false);
        boardFrame.dispose();
        controlFrame.setVisible(false);
        controlFrame.dispose();
    }
}
