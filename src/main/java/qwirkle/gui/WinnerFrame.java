package qwirkle.gui;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WinnerFrame extends JFrame implements ActionListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private JButton newgame;
    GUIView view;

	/**.
     * Initialise a frame to show that there is a winner
     */

    public WinnerFrame(GUIView view, boolean hasWon, int playerNumber) {
        super("GameOver");
        this.view = view;

        Container panel = getContentPane();
        panel.setLayout(new GridLayout(3,1));

        JLabel label = new JLabel();
        if (hasWon) {
            label.setText(String.format("You have won with %d points", view.getGame().getLocalPlayer().getScore()));
        } else {
            label.setText(String.format("Player %d has won with score %d", playerNumber, view.getGame().getPlayer(playerNumber).getScore()));
        }
        panel.add(label);

        JPanel players = new JPanel();
        players.setLayout(new GridLayout(view.getGame().getPlayers().size(), 2));
        for (int i = 0; i < view.getGame().getPlayers().size(); i++) {
            JLabel name = new JLabel(view.getGame().getPlayer(i).name);
            JLabel score = new JLabel("" +view.getGame().getPlayer(i).getScore());
            players.add(name);
            players.add(score);
        }

        panel.add(players);

        newgame = new JButton("Start new game!");
        newgame.addActionListener(this);

        panel.add(newgame);

        setSize(600, 100);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        view.reset();
        setVisible(false);
        dispose();
    }
}
