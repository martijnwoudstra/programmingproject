package qwirkle;

import qwirkle.gui.GUIView;
import qwirkle.lib.Protocol;
import qwirkle.network.ClientConnector;
import qwirkle.player.Player;
import qwirkle.player.local.ComputerPlayer;
import qwirkle.player.local.HumanPlayer;
import qwirkle.player.local.LocalPlayer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by martijn on 11-1-16.
 */
public class Game {

    private ClientConnector clientConnector;
    private Board board;
    private GUIView view;
    private int ownPlayerIdentifier;
    private Map<Integer, Player> players;
    private int currentPlayerIdentifier;
    private boolean ai;

    /**.
     * Creates a game object.
     * Sets {@link qwirkle.Game#players} to a new {@link java.util.Map},
     * sets {@link qwirkle.Game#board} to a new {@link qwirkle.Board}
     *      with {@link qwirkle.Game} as parameter,
     * sets {@link qwirkle.Game#view} to a new {@link GUIView}
     */
    public Game() {
        players = new HashMap<Integer, Player>();
        board = new Board(this);
        view = new GUIView(this);
    }

    /**.
     * Connects to a server.
     * Adds view as observer, and sends the login message to the server
     * @param host
     *      String host adress
     * @param port
     *      Int port number
     * @param name
     *      String playername.
     * @throws IOException
     *      Inherited from creating a {@link qwirkle.network.ClientConnector}
     */
    public void connectToServer(String host, int port, String name,
                                boolean isAI) throws IOException {
        clientConnector = new ClientConnector(this, host, port);
        clientConnector.addObserver(view);
        clientConnector.sendToServer(String.format("%s %s", Protocol.Client.LOGIN, name));
        this.ai = isAI;
    }

    /**.
     * Returns the ClientConnector object
     * @return {@link qwirkle.network.ClientConnector} object
     */
    public ClientConnector getClientConnector() {
        return clientConnector;
    }

    /**.
     * Adds a player to the local game.
     * Does this by adding it to a map.
     * If the player is an instance of human player, creates a human player instead.
     * @param number
     *      Int player number
     * @param player
     *      {@link qwirkle.player.Player} object of the player to add.
     */
    public void addPlayer(int number, Player player) {
        if (player instanceof LocalPlayer) {
            setOwnPlayerIdentifier(number);
        }
        players.put(number, player);
    }

    /**.
     * Removes the player from the local game
     * @param number
     *      Int player number
     */
    public void removePlayer(int number) {
        players.remove(number);
    }

    /**.
     * Returns a map of all players in the game.
     * @return Map of all players in the game.
     */
    public Map<Integer, Player> getPlayers() {
        return players;
    }

    /**.
     * Sets the current player identifier
     * This is basically a value that stores the player number of the current player.
     * @param currentPlayerIdentifier
     *      Int current player identifier.
     */
    public void setCurrentPlayerIdentifier(int currentPlayerIdentifier) {
        this.currentPlayerIdentifier = currentPlayerIdentifier;
    }

    /**.
     * Returns the currentPlayerIdentifier
     * @see qwirkle.Game#setCurrentPlayerIdentifier(int);
     * @return Int current player identifier.
     */
    public int getCurrentPlayerIdentifier() {
        return currentPlayerIdentifier;
    }

    /**.
     * Sets the human player identifier
     * This is basically a value that stores the player number of the human player.
     * @param humanPlayerIdentifier
     *      Int human player identifier.
     */
    public void setOwnPlayerIdentifier(int humanPlayerIdentifier) {
        this.ownPlayerIdentifier = humanPlayerIdentifier;
    }

    /**.
     * Returns the currentPlayerIdentifier
     * @see qwirkle.Game#setOwnPlayerIdentifier(int);
     * @return Int human player identifier.
     */
    public int getOwnPlayerIdentifier() {
        return ownPlayerIdentifier;
    }


    /**.
     * Returns the Player object of the human player
     * @see qwirkle.Game#setOwnPlayerIdentifier(int)
     * @return {@link qwirkle.player.Player} object of the human player,
     *      casted to {@link qwirkle.player.local.HumanPlayer}
     */
    public HumanPlayer getHumanPlayer() {
        return players.get(ownPlayerIdentifier) instanceof HumanPlayer ?
                (HumanPlayer) players.get(ownPlayerIdentifier) : null;
    }

    /**.
     * Returns the Player object of the computer player
     * @see qwirkle.Game#setOwnPlayerIdentifier(int)
     * @return {@link qwirkle.player.Player} object of the human player,
     *      casted to {@link qwirkle.player.local.ComputerPlayer}
     */
    public ComputerPlayer getComputerPlayer() {
        return players.get(ownPlayerIdentifier) instanceof ComputerPlayer ?
                (ComputerPlayer) players.get(ownPlayerIdentifier) : null;
    }

    /**.
     * Returns the Player object of the player
     * @see qwirkle.Game#setOwnPlayerIdentifier(int)
     * @return {@link qwirkle.player.Player} object of the human player,
     *      casted to {@link qwirkle.player.local.LocalPlayer}
     */
    public LocalPlayer getLocalPlayer() {
        return (LocalPlayer) players.get(ownPlayerIdentifier);
    }


    /**.
     * Returns the board object belonging to the current game
     * @see qwirkle.Board
     * @return {@link qwirkle.Board} object of the current game.
     */
    public Board getBoard() {
        return board;
    }

    /**.
     * Returns whether the player is an {@link qwirkle.ai.Ai} or not
     * @return Boolean true if player is an {@link qwirkle.ai.Ai}, otherwise false.
     */
    public boolean isAi() {
        return ai;
    }

    /**.
     * Returns the current {@link qwirkle.player.Player}
     * @return {@link qwirkle.player.Player} current player.
     */
    public Player getCurrentPlayer() {
        return players.get(currentPlayerIdentifier);
    }

    /**.
     * Gets the {@link qwirkle.player.Player} with the player number.
     * @param playerNumber
     *      Int number of the playerr
     * @return {@link qwirkle.player.Player} Current Player.
     */
    public Player getPlayer(int playerNumber) {
        return players.get(playerNumber);
    }

    public void reset(){
        players = new HashMap<Integer, Player>();
        board = new Board(this);
    }
}
