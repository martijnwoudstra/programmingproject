package qwirkle.exception;

import qwirkle.tiles.Tile;

/**
 * Created by martijn on 11-1-16.
 */
public class InvalidMoveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Tile tile;
	private int row;
	private int col;
	private String message = "";

	public InvalidMoveException(Tile tile, int row, int col) {
		this.tile = tile;
		this.row = row;
		this.col = col;
		print();
	}

	public InvalidMoveException(String s) {
		message = s;
		print();
	}

	/**
	 * Prints the error message. If you gave a custom string, it prints that,
	 * otherwise creates a string based on the tile and coordinates.
	 */
	private void print() {
		if (message.equals("")) {
			System.out.println("Invalid move for " + tile.toString() 
			    + " at row " + row + " and column " + col);
		} else {
			System.out.println(message);
		}
	}
}
