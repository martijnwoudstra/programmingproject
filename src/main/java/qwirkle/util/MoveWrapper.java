package qwirkle.util;

import qwirkle.tiles.Tile;

import java.util.List;

/**
 * Created by martijn on 19-1-16.
 */

/**.
 * Wrapper class for a collection of a {@link qwirkle.tiles.Tile} and a 
 * 		{@link java.util.List} of {@link Integer}
 */
public class MoveWrapper {
    private Tile tile;
    private List<Integer> coordinates;

    @Override
    public String toString() {
        return String.format(" %s %d %d", getTile().toString(), 
        		  getCoordinates().get(0), getCoordinates().get(1));
    }

    /**
     * Gets the first value of the wrapper.
     * This is the tile.
     * @return {@link qwirkle.tiles.Tile} object
     */
    public Tile getTile() {
        return tile;
    }

    /**
     * Sets the first value of the wrapper.
     * This is the tile.
     * @param tile
     *      {@link qwirkle.tiles.Tile} object
     */
    public void setTile(Tile tile) {
        this.tile = tile;
    }

    /**
     * Returns the coordinates of the wrapper.
     * {@link {@link MoveWrapper#coordinates}.get(0)} returns row
     * {@link {@link MoveWrapper#coordinates}.get(1)} returns col
     * @return {@link java.util.List<Integer>} with the coordinates
     */
    public List<Integer> getCoordinates() {
        return coordinates;
    }

    /**.
     * Sets the coordinates
     * List should have
     *      row in the first value
     *      col in the second value
     *
     * @param coordinates
     *      List with coordinates of the tile
     */
    public void setCoordinates(List<Integer> coordinates) {
        this.coordinates = coordinates;
    }
}
