package qwirkle.util;

import java.util.StringTokenizer;

/**
 * Created by martijn on 11-1-16.
 */

public class Utils {

    /**.
     * Returns the words in the String
     * @param line
     *      String where words should be counted.
     * @return Int how many words in the string
     */
    public static int countWords(String line) {
        StringTokenizer st = new StringTokenizer(line);
        return st.countTokens();
    }
}
