package qwirkle;


import qwirkle.network.server.Server;

/**
 * Created by martijn on 11-1-16.
 */
public class Start {


    public void startClient() {
    	new Game();   
    }

    public void startServer() {
        new Server();
    }
}