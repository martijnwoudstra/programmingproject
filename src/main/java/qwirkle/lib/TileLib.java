package qwirkle.lib;

/**
 * Created by martijn on 11-1-16.
 */

/**.
 * Library for characters beloning to {@link qwirkle.tiles.Color} and {@link qwirkle.tiles.Shape}
 */
public class TileLib {
    public static final char RED_CHAR = 'R';
    public static final char ORANGE_CHAR = 'O';
    public static final char BLUE_CHAR = 'B';
    public static final char YELLOW_CHAR = 'Y';
    public static final char GREEN_CHAR = 'G';
    public static final char PURPLE_CHAR = 'P';

    public static final char CIRCLE_CHAR = 'o';
    public static final char DIAMOND_CHAR = 'd';
    public static final char SQUARE_CHAR = 's';
    public static final char CLOVER_CHAR = 'c';
    public static final char CROSS_CHAR = 'x';
    public static final char STAR_CHAR = '*';

    public static final char NONE_CHAR = '-';
}