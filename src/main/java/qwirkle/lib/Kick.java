package qwirkle.lib;

/**.
 * Created by matthijs on 11-1-16.
 */

/**.
 * Class to define kick reasons
 */
public class Kick {
    public static final String INVALID_NAME = "Not valid name, name should "
    		  + "only contain a-z and length between 1 and 16 chars";
    public static final String EMPTY_STACK = "Not allowed to swap if stack is empty.";
    public static final String INVALID_MOVE = "No valid move!";
    public static final String INVALID_COMMAND = "Not a valid command!";
    public static final String NOT_YOUR_TURN = "It was not your turn!";
    public static final String CONNECTION_LOST = "Connection with the client is lost!";
}
