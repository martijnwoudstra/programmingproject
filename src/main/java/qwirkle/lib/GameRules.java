package qwirkle.lib;

/**.
 * Created by martijn on 11-1-16.
 */

/**.
 * Game rules.
 */
public class GameRules {
    public static final int BOARD_COLS = 185;
    public static final int BOARD_ROWS = BOARD_COLS;
    public static final int MAX_HAND_SIZE = 6;
    public static final int MAX_PLAYERS = 4;
    public static final int MAX_STACK_SIZE = 108;
}
