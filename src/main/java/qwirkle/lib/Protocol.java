package qwirkle.lib;

/**.
 * Created by matthijs
 */
public class Protocol {

    /**.
     * Commands that the server can pass
     */
    public class Server {
        public static final String LOGIN_ACCEPTED = "WELCOME";
        public static final String USERS = "NAMES";
        public static final String NEXT = "NEXT";
        public static final String NEW = "NEW";
        public static final String TURN = "TURN";
        public static final String KICK = "KICK";
        public static final String WINNER = "WINNER";

    }

    /**.
     * Commands that the client can pass
     */
    public class Client {
        public static final String LOGIN = "HELLO";
        public static final String MOVE = "MOVE";
        public static final String SWAP = "SWAP";
    }
}
