package qwirkle.network;

import qwirkle.Game;
import qwirkle.exception.InvalidMoveException;
import qwirkle.lib.GameRules;
import qwirkle.lib.Protocol;
import qwirkle.move.PlaceMove;
import qwirkle.player.Player;
import qwirkle.player.local.ComputerPlayer;
import qwirkle.player.local.HumanPlayer;
import qwirkle.player.network.NetworkPlayer;
import qwirkle.tiles.Color;
import qwirkle.tiles.Shape;
import qwirkle.tiles.Tile;
import qwirkle.util.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Observable;
import java.util.Scanner;

/**
 * Created by martijn on 11-1-16.
 */
public class ClientConnector extends Observable implements Runnable {

	private Socket socket;
	private PrintWriter printWriter;
	private BufferedReader bufferedReaderStream;
	private Thread t;
	private Game game;

	/**
	 * Connects to a server with a host, and a port. Creates a
	 * {@link qwirkle.network.ClientConnector} object.
	 * 
	 * @param game
	 *            {@link qwirkle.Game} object
	 * @param host
	 *            String host you want to connect to
	 * @param port
	 *            Int port you want to connect to
	 * @throws IOException
	 *             If connecting to the server fails
	 */
	public ClientConnector(Game game, String host, int port) throws IOException {
		try {
			this.game = game;
			connectToServer(host, port);
			t = new Thread(this);
			t.start();
		} catch (IOException e) {
			throw e;
		}
	}

	/**
	 * Connects to a server.
	 * 
	 * @param host
	 *            String host you want to connect to
	 * @param port
	 *            Int port you want to connect to.
	 * @throws IOException
	 *             If connecting to the server fails.
	 */
	private void connectToServer(String host, int port) throws IOException {
		socket = new Socket(host, port);
		printWriter = new PrintWriter(socket.getOutputStream(), true);
		bufferedReaderStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	}

	/**
	 * . Sends a message to the server through the
	 * {@link qwirkle.network.ClientConnector#printWriter}
	 * 
	 * @param msg
	 *            String message you want to send.
	 */
	public void sendToServer(String msg) {
		printWriter.println(msg);
	}

	@Override
	public void run() {
		String line;
		try {
			while ((line = bufferedReaderStream.readLine()) != null) {
				extractCommand(line);
			}
			close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Extracts the command from the server. This is read from the
	 * {@link qwirkle.network.ClientConnector#bufferedReaderStream} Breaks this
	 * down using the identifiers definined in {@link qwirkle.lib.Protocol}
	 * 
	 * @param line
	 *            String line read from
	 *            {@link qwirkle.network.ClientConnector#bufferedReaderStream}
	 */
	private void extractCommand(String line) {
		System.out.println("Server -> Client: " + line);
		Scanner scanner = new Scanner(line);
		int playerNumber;
		String identifier = scanner.next();
		switch (identifier) {
		/*
		 * If the connection is accepted, we store our playername and
		 * playernumber. We add a new humanplayer to our local game. We notify
		 * the observers
		 */
		case Protocol.Server.LOGIN_ACCEPTED:
			String myName = scanner.next();
			playerNumber = scanner.nextInt();
			if (game.isAi()) {
				game.addPlayer(playerNumber, new ComputerPlayer(myName, playerNumber));
			} else {
				game.addPlayer(playerNumber, new HumanPlayer(myName, playerNumber));
			}
			setChanged();
			notifyObservers(identifier);
			break;
		/*
		 * The game has started, and the users are determined. We store these
		 * players with their numbers and names.
		 */
		case Protocol.Server.USERS:
			boolean finished = false;
			int players = 0;
			while (!finished) {
				String name = scanner.next();
				playerNumber = scanner.nextInt();
				if (!(playerNumber == game.getLocalPlayer().number)) {
					Player player = new NetworkPlayer(name);
					game.addPlayer(playerNumber, player);
					players++;
					setChanged();
					notifyObservers(player);

				}
				setChanged();
				notifyObservers(game.getHumanPlayer());
				finished = scanner.hasNextInt();
			}
			if (game.getLocalPlayer() instanceof ComputerPlayer) {
				game.getComputerPlayer().setAITime(new Long(scanner.nextInt()));
			}
			game.getBoard().setStackSize(GameRules.MAX_STACK_SIZE - (players * 6));
			break;
		/*
		 * Next player Shifts the currentPlayerIdentifier.
		 */
		case Protocol.Server.NEXT:
			game.getBoard().deepCopy();
			game.setCurrentPlayerIdentifier(scanner.nextInt());
			// If it's your turn and you're ai
			if (game.getCurrentPlayerIdentifier() == game.getOwnPlayerIdentifier()
					&& game.getComputerPlayer() != null) {
				game.getComputerPlayer().determineMove(game.getBoard());
			}
			setChanged();
			notifyObservers(line);
			break;
		/*
		 * We received new tiles. Extract the tiles from the string, and add
		 * them to the player.
		 */
		case Protocol.Server.NEW:
			// String newTileLine = scanner.nextLine();
			for (int i = 0; i < Utils.countWords(line) - 1; i++) {
				String tile = scanner.next();
				Tile newTile = new Tile(Color.generateColorFromChar(tile.charAt(0)),
						Shape.generateShapeFromChar(tile.charAt(1)));
				game.getLocalPlayer().addTiles(newTile);
			}
			setChanged();
			notifyObservers(Protocol.Server.NEW);
			break;
		/*
		 * Someone has made a move. If move is empty, do nothing Else extract
		 * the move from string, and execute the move
		 */
		case Protocol.Server.TURN:
			playerNumber = scanner.nextInt();
			String turnString = scanner.nextLine();
			if (!line.contains("empty")) {
				PlaceMove move = null;
				try {
					move = PlaceMove.generateMoveFromString(turnString.substring(1), game.getBoard());
					int size = move.getMoves().size();
					game.getBoard().decreaseStackSize(size);
				} catch (InvalidMoveException e) {
					e.printStackTrace();
				}
				move.doOthersMove(playerNumber);
				setChanged();
				notifyObservers(line);
				game.getBoard().deepCopy();
				setChanged();
				notifyObservers(String.format("UPDATE %d %d", playerNumber, game.getPlayer(playerNumber).getScore()));
			}
			break;
		/*
		 * Someone has been kicked. Remove player.
		 */
		case Protocol.Server.KICK:
			playerNumber = scanner.nextInt();
			if (playerNumber != game.getOwnPlayerIdentifier()) {
				game.removePlayer(playerNumber);
			}
			int tilesBackToStack = scanner.nextInt();
			game.getBoard().setStackSize(game.getBoard().getStackSize() + tilesBackToStack);
			String reason = scanner.nextLine();
			setChanged();
			notifyObservers(String.format("%s %d %s", Protocol.Server.KICK, playerNumber, reason));
			break;
		/*
		 * Winner is determined Notify.
		 */
		case Protocol.Server.WINNER:
			playerNumber = scanner.nextInt();
			setChanged();
			notifyObservers(String.format("%s %d", Protocol.Server.WINNER, playerNumber));
			break;
		}
		scanner.close();
	}

	private void close() {
		setChanged();
		notifyObservers("NOCONNECTION");
	}
}
