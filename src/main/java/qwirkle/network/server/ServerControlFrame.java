package qwirkle.network.server;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ServerControlFrame extends JFrame implements ActionListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**.
     * Construeert een frame waarmee de laatste game op de server gestart kan worden
     */
    Server server;
    JLabel error = new JLabel();
    JButton start = new JButton();
    ArrayList<Integer> swapIndexHand = new ArrayList<Integer>();

    public ServerControlFrame(Server server) {
        super("Server Controller");
        this.server = server;

        Container panel = getContentPane();
        panel.setLayout(new BorderLayout());

        // Error message
        error = new JLabel();
        panel.add(BorderLayout.NORTH, error);


        start = new JButton("Start game!");
        start.addActionListener(this);

        panel.add(BorderLayout.SOUTH, start);


        setSize(400,  200);
        // unable to close
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    /**.
     * Show a error message on the view
     * @param text
     *          String text that should be displayed
     */
    public void error(String text) {
        error.setText("Error: " + text);
    }

    /**.
     * Show a message on the view
     * @param text
     *          String text that should be displayed
     */
    public void message(String text) {
        error.setText(text);
    }

    public void actionPerformed(ActionEvent e) {
        if (server.startGame()) {
            message("Game start");
        } else {
            error("No inactive game");
        }

    }


}
