package qwirkle.network.server;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ServerStartFrame extends JFrame implements ActionListener{
    /**
     * Construeert een frame waarmee de laatste game op de server gestart kan worden
     */
    Server server;
    JLabel error = new JLabel();
    JButton start = new JButton();
    JTextField port = new JTextField();
    JTextField aitime = new JTextField();
    ArrayList<Integer> swapIndexHand = new ArrayList<Integer>();

    public ServerStartFrame(Server server) {
        super("Start Server");
        this.server = server;

        Container panel = getContentPane();
        panel.setLayout(new BorderLayout());

        // Error message
        error = new JLabel();
        panel.add(BorderLayout.NORTH, error);

        JPanel pane = new JPanel();


        JLabel porttext = new JLabel("Port");
        port = new JTextField(10);

        pane.add(porttext);
        pane.add(port);

        JPanel ai = new JPanel();
        JLabel aiText = new JLabel("AiTime (ms)");
        aitime = new JTextField(10);

        ai.add(aiText);
        ai.add(aitime);


        panel.add(BorderLayout.NORTH, pane);
        panel.add(BorderLayout.CENTER, ai);

        start = new JButton("Start server!");
        start.addActionListener(this);

        panel.add(BorderLayout.SOUTH, start);


        setSize(400,200);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    /**
     * Show a error message on the view
     * @param text
     *          String text that should be displayed
     */
    public void error(String text){
        error.setText("Error: " + text);
    }

    /**
     * Show a message on the view
     * @param text
     *          String text that should be displayed
     */
    public void message(String text){
        error.setText(text);
    }

    public void actionPerformed(ActionEvent e){
        if(port.getText().length() > 4){
            error("Please enter a port with 4 digits");
        } else if(!(aitime.getText().length() > 0)){
            error("Please enter an ai time");
        } else {
            try {
                int p = Integer.parseInt(port.getText());
                int ai = Integer.parseInt(aitime.getText());
                if(server.startServer(p, ai)){
                    message("Server start");
                    new ServerControlFrame(server);
                    setVisible(false);
                    dispose();
                } else {
                    error("Port already in use!");
                }
            } catch (NumberFormatException ex){
                error("Please enter a digit");
            }
        }



    }


}
