package qwirkle.network.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Server.
 */
public class Server extends Thread {
	private int port;
	private ServerSocket serverSock;
	private List<ServerGame> games = new ArrayList<ServerGame>();
	private int aitime;
    public boolean active = true;

    public Server() {
        new ServerStartFrame(this);
    }

    /**
     * Initialse the server
     */
    public void run() {
        games.add(new ServerGame(aitime));
        try {
            System.out.println("Waiting for clients...");
            while (active) {
                Socket sock = serverSock.accept();
                // get the last not active game
                ServerGame newestGame = games.get(games.size() - 1);
                if (newestGame.isActive()) {
                    newestGame = new ServerGame(aitime);
                    games.add(newestGame);
                }
                // create new thread to handle
                ClientHandler c = new ClientHandler(newestGame, sock);
                newestGame.addHandler(c);
                c.start();
            }
        }
        catch(IOException e){
            System.err.println(e);
        }
    }

	/**.
	 * Start the non active game
	 * 
	 * @return returns a boolean if there is a non active game
	 */
	public boolean startGame() {
		ServerGame newestGame = games.get(games.size() - 1);
		if (!newestGame.isActive() && newestGame.getThreads().size() > 1) {
			newestGame.startGame();
			return true;
		}
		return false;
	}

    /**
     * Start the server
     * @param p the port on which the server should listen
     * @param ai the ai time that the server should pass to the client
     * @return returns a boolean if the server is started
     */
    public boolean startServer(int p, int ai){
        // create server socket
        try {
            aitime = ai;
            serverSock = new ServerSocket(p);
            start();
            return true;
        } catch (IOException e) {
            System.err.println(e);
            return false;
        }


    }

    /**
     * Shutdowns the server
     */
    public void shutdown(){
        try {
            serverSock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<ServerGame> getGames() {
        return games;
    }
}