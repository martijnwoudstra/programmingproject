package qwirkle.network.server;

import qwirkle.Board;
import qwirkle.exception.InvalidMoveException;
import qwirkle.lib.GameRules;
import qwirkle.lib.Protocol;
import qwirkle.move.PlaceMove;
import qwirkle.tiles.Color;
import qwirkle.tiles.Shape;
import qwirkle.tiles.Tile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static qwirkle.tiles.Color.getColors;
import static qwirkle.tiles.Shape.getShapes;

/**
 * Created by matthijs on 20-1-16.
 */
public class ServerGame extends Thread {

	private List<ClientHandler> threads = new ArrayList<ClientHandler>();
	private int currentClient; // The index of the current client, not the clientNumber
	private Board board;
	private List<Tile> stack = new ArrayList<Tile>();
	private boolean isActive = false;
	private int notAbleToDoMove = 0; // int to count how many players were not able to do a move
	private boolean hasEnd = false;
	private int aitime;

	public ServerGame(int ai_time) {
		createStack();
		aitime = ai_time;
	}

	/**
	 * . Start the game
	 */
	//@ requires getThreads().size() > 1;
	public synchronized void startGame() {
		System.out.println("Starting game...");
		isActive = true;
		String broadcastString = "";
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).getClientName() != null){
				broadcastString += " " + threads.get(i).getClientName();
				broadcastString += " " + threads.get(i).getClientNumber();
				for (int t = 0; t < GameRules.MAX_HAND_SIZE; t++) {
					Tile ti = getTileFromStack();
					threads.get(i).addToHand(ti);
				}
			}
		}
		// create empty board
		board = new Board(); // todo board has no game
		// Inform clients about the users
		broadcast(String.format("%s%s %d", Protocol.Server.USERS, broadcastString, aitime));
		try {
			sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		getBestHand();

		// inform over the new hand
		for (int i = 0; i < threads.size(); i++) {
			String tiles = "";
			for (int j = 0; j < threads.get(i).getHand().size(); j++) {
				tiles += threads.get(i).getHand().get(j).toString() + " ";
			}
			threads.get(i).sendMessage(String.format("%s %s", Protocol.Server.NEW, tiles));
		}
		nextClient();
	}

	/**
	 * Sends a message using the collection of connected ClientHandlers to all
	 * connected Clients.
	 *
	 * @param msg
	 *            message that is send
	 */
	public synchronized void broadcast(String msg) {
		System.out.println("Server -> Client " + msg);
		for (int i = 0; i < threads.size(); i++) {
			threads.get(i).sendMessage(msg);
		}
	}

	/**
	 * Add a ClientHandler to the collection of ClientHandlers.
	 *
	 * @param handler
	 *            ClientHandler that will be added
	 */
	public synchronized void addHandler(ClientHandler handler) {
		threads.add(handler);
		handler.setClientNumber(threads.size()-1);
	}

	/**
	 * Remove a ClientHandler from the collection of ClientHanlders.
	 *
	 * @param handler
	 *            ClientHandler that will be removed
	 */
	public synchronized void removeHandler(ClientHandler handler) {
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i) == handler) {
				threads.remove(i);
			}
		}
	}

	/**
	 * . Create all tiles in the stack (6*6 tiles combinates and all 3 times)
	 */
	public synchronized void createStack() {
		stack = new ArrayList<Tile>();
		Color[] colors = getColors();
		Shape[] shapes = getShapes();
		for (int i = 0; i < 3; i++) {
			// loop through colors
			for (Color c : colors) {
				// loop through shapes
				for (Shape s : shapes) {
					stack.add(new Tile(c, s));
				}
			}
		}
	}

	public synchronized List<Tile> getStack() {
		return stack;
	}

	//@ ensures getStack()().size() == \old(getStack()().size())-1
	public synchronized Tile getTileFromStack() {
		int i = (int) (Math.random() * stack.size());
		Tile t = stack.get(i);
		stack.remove(i);
		return t;
	}

	public synchronized ClientHandler getCurrentClient() {
		return threads.get(currentClient);
	}

	public synchronized int getCurrentClientNumber(){
		return currentClient;
	}

	public synchronized void nextClient() {
		if (!checkWinner()) {
			currentClient = (currentClient + 1) % threads.size();
			// check if this user can do something
			if (getCurrentClient().canDoMove()) {
				notAbleToDoMove = 0;
				// checkwinner
				broadcast(String.format("%s %d", Protocol.Server.NEXT, getCurrentClient().getClientNumber()));
			} else {
				// Inform that this client skips
				broadcast(String.format("%s %d %s", Protocol.Server.TURN, getCurrentClient().getClientNumber(), "empty"));
				notAbleToDoMove++;
			}
		}

	}

	/**.
	 * Swap two tiles, exchange 1 tile from client for 1 tile from stack
	 * 
	 * @param t
	 *            Tile t is the tile that is returned to stack
	 * @return Return the tile that is given back to the client
	 */
	//@ ensures getStack()().size() == \old(getStack()().size())
	public synchronized Tile swap(Tile t) {
		int i = (int) (Math.random() * stack.size());
		Tile ans = stack.get(i);
		stack.set(i, t);
		return ans;
	}

	/**
	 * . Methode to put tiles back to stack if a user is kicked
	 * 
	 * @param hand
	 *            The hand of the client
	 */
	//@ ensures getStack().size() == \old(getStack().size())+hand.size()
	public synchronized void tilesBackToStack(ArrayList<Tile> hand) {
		for (int i = 0; i < hand.size(); i++) {
			stack.add(hand.get(i));
		}
	}

	/**
	 * Check if there is a winner
	 * first checks if the stack size is zero, after this checks if there is a player with a empty hand
	 * If there is a player with an empty hand, the player with the highest score wins
	 * Also has a winner if there is only 1 thread left
	 * @return Returns if there is a winner.
     */
	//@ ensures (\forall int i; (i >= 0 & i < threads.size()) ; (threads.get(i).getScore() <= highestScore);
	public synchronized boolean checkWinner() {
		int highestScore = 0;
		ClientHandler bestPlayer = null;
		boolean hasWinner = false;
		if (stack.size() == 0) {
			System.out.println("Stack empty");
			for (int i = 0; i < threads.size(); i++) {
				if (threads.get(i).getScore() > highestScore) {
					highestScore = threads.get(i).getScore();
					bestPlayer = threads.get(i);
				}
				// if there is a player with an empty hand
				if (threads.get(i).getHand().size() == 0) {
					System.out.println("Empyty hand!");
					hasWinner = true;
				} else {
					System.out.println("none empty hand!" + threads.get(i).getHand().toString());
				}
			}
			// if nobody can do something
			if (notAbleToDoMove == threads.size()) {
				hasWinner = true;
			}
		} else if(threads.size() == 1){
			hasWinner = true;
			bestPlayer = threads.get(0);
		}
		if (hasWinner) {
			System.out.println("Winner");
			broadcast(String.format("%s %s", Protocol.Server.WINNER, bestPlayer.getClientNumber()));
			reset();
		}
		return hasWinner;
	}

	public synchronized void reset() {
		hasEnd = true;
		// reset all clienthandlers
		for (int i = 0; i < threads.size(); i++) {
			threads.get(i).shutdown();
			threads.remove(i);
		}
		stack = new ArrayList<Tile>();
	}

	/**
	 * Get the player with the best hand
	 *
	 * @return Returns the clientnumber of the player with the best hand
     */
	public synchronized int getBestHand() {

		ClientHandler bestPlayer = null;
		Set<Tile> bestSet = new HashSet<Tile>();

		for (ClientHandler client : threads) {
			ArrayList<Tile> hand = client.getHand();
			Set<Tile> bestLocal = new HashSet<Tile>();
			// Set for colors
			ArrayList<Set<Tile>> colors = new ArrayList<Set<Tile>>();
			ArrayList<Set<String>> colorsString = new ArrayList<Set<String>>();
			for (int i = 0; i < 6; i++) {
				colors.add(new HashSet<Tile>());
				colorsString.add(new HashSet<String>());
			}

			// Set for colors
			// Set for colors
			ArrayList<Set<Tile>> shapes = new ArrayList<Set<Tile>>();
			ArrayList<Set<String>> shapesString = new ArrayList<Set<String>>();
			for (int i = 0; i < 6; i++) {
				shapes.add(new HashSet<Tile>());
				shapesString.add(new HashSet<String>());
			}

			for (Tile t : hand) {
				switch (t.getColor()) {
					case RED:
						if (!colorsString.get(0).contains(t.toString())) {
							colors.get(0).add(t);
							colorsString.get(0).add(t.toString());
						}
						break;
					case ORANGE:
						if (!colorsString.get(1).contains(t.toString())) {
							colors.get(1).add(t);
							colorsString.get(1).add(t.toString());
						}
						break;
					case BLUE:
						if (!colorsString.get(2).contains(t.toString())) {
							colors.get(2).add(t);
							colorsString.get(2).add(t.toString());
						}
						break;
					case PURPLE:
						if (!colorsString.get(3).contains(t.toString())) {
							colors.get(3).add(t);
							colorsString.get(3).add(t.toString());
						}
						break;
					case YELLOW:
						if (!colorsString.get(4).contains(t.toString())) {
							colors.get(4).add(t);
							colorsString.get(4).add(t.toString());
						}
						break;
					case GREEN:
						if (!colorsString.get(5).contains(t.toString())) {
							colors.get(5).add(t);
							colorsString.get(5).add(t.toString());
						}
						break;
					default:
						break;
				}

				switch (t.getShape()) {
					case CIRCLE:
						if (!shapesString.get(0).contains(t.toString())) {
							shapes.get(0).add(t);
							shapesString.get(0).add(t.toString());
						}
						break;
					case DIAMOND:
						if (!shapesString.get(1).contains(t.toString())) {
							shapes.get(1).add(t);
							shapesString.get(1).add(t.toString());
						}
						break;
					case SQUARE:
						if (!shapesString.get(2).contains(t.toString())) {
							shapes.get(2).add(t);
							shapesString.get(2).add(t.toString());
						}
						break;
					case CLOVER:
						if (!shapesString.get(3).contains(t.toString())) {
							shapes.get(3).add(t);
							shapesString.get(3).add(t.toString());
						}
						break;
					case CROSS:
						if (!shapesString.get(4).contains(t.toString())) {
							shapes.get(4).add(t);
							shapesString.get(4).add(t.toString());
						}
						break;
					case STAR:
						if (!shapesString.get(5).contains(t.toString())) {
							shapes.get(5).add(t);
							shapesString.get(5).add(t.toString());
						}
						break;
					default:
						break;
				}
			}

			// get bigest color set
			for (int i = 0; i < colors.size(); i++) {
				if (colors.get(i).size() > bestLocal.size()) {
					bestLocal = colors.get(i);
				}
			}
			// overwrite with biggest shape set if there is a bigger shape set
			for (int i = 0; i < shapes.size(); i++) {
				if (shapes.get(i).size() > bestLocal.size()) {
					bestLocal = shapes.get(i);
				}
			}
			if (bestLocal.size() > bestSet.size()) {
				bestSet = bestLocal;
				bestPlayer = client;
			}
		}
		// do the set on the board
		System.out.println("Best set: " + bestSet.toString());
		PlaceMove pm = new PlaceMove(board);
		int col = 91;
		for (Tile t : bestSet) {
			try {
				pm.addPlaceMove(t, 91, col);
				col++;
			} catch (InvalidMoveException e) {
				System.err.println(e);
			}
		}
		currentClient = bestPlayer.getClientNumber();
		bestPlayer.doMove(pm, true);
		bestPlayer.setScore(bestPlayer.getScore() +  pm.getScore());
		System.out.println("User " + bestPlayer.getClientNumber() + " had the best hand!");
		return bestPlayer.getClientNumber();
	}

	public synchronized Board getBoard() {
		return board;
	}

	public synchronized List<ClientHandler> getThreads() {
		return threads;
	}

	public synchronized boolean isActive() {
		return isActive;
	}

	public boolean isEnd() {
		return hasEnd;
	}
}
