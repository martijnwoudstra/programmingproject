package qwirkle.network.server;

import qwirkle.Board;
import qwirkle.exception.InvalidMoveException;
import qwirkle.lib.Kick;
import qwirkle.lib.Protocol;
import qwirkle.move.PlaceMove;
import qwirkle.tiles.Color;
import qwirkle.tiles.Shape;
import qwirkle.tiles.Tile;
import qwirkle.util.Utils;
import qwirkle.util.MoveWrapper;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * ClientHandler.
 */
public class ClientHandler extends Thread {
    private ServerGame game;
    private BufferedReader in;
    private BufferedWriter out;
    private Socket sock;
    private String clientName;
    private int clientNumber;
    private int score;
    private ArrayList<Tile> hand = new ArrayList<Tile>();

    /**.
     * Constructs a ClientHandler object
     * Save all the input/output streams
     */
    //@ requires gameArg != null && sockArg != null;
    public ClientHandler(ServerGame gameArg, Socket sockArg) throws IOException {
        in = new BufferedReader(new InputStreamReader(sockArg.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(sockArg.getOutputStream()));
        this.game = gameArg;
        sock = sockArg;
    }

    /**.
     * This methode will read all the data send from the client to the server
     */
    public void run() {
        boolean isActive = true;
        while (isActive) {
            try {
                String theLine;
                theLine = in.readLine();
                if (theLine != null) {
                    readMessage(theLine);
                } else {
                    isActive = false;
                    connectionLost();
                }

            } catch (IOException e) {
                isActive = false;
                connectionLost();
            }
        }

    }

    /**.
     * Read a command which is send by the client
     * @param msg String msg the command that is send by the client
     */
    public void readMessage(String msg) {
        System.out.println("Client -> Server " + msg);
        Scanner scan = new Scanner(msg);
        String line = scan.next();
        switch (line) {
            // client send login name
            case Protocol.Client.LOGIN:
                String pattern = "[A-Za-z]{1,16}";
                String name = scan.next();
                if (name.matches(pattern)) {
                    setClientName(name);
                    sendMessage(String.format("%s %s %d", Protocol.Server.LOGIN_ACCEPTED, 
                    		  clientName, clientNumber));
                    if (game.getThreads().size() == 4){
                        game.startGame();
                    }

                } else {
                    kick(Kick.INVALID_NAME);
                }
                break;
            // client send a new move
            case Protocol.Client.MOVE:
                if(msg.contains("empty")){
                    System.err.println("No valid line! " + line);
                }
                else if (game.getCurrentClient() == this) {
                    try {
                        PlaceMove move = PlaceMove.
                        		  generateMoveFromString(scan.nextLine().substring(1), 
                        				game.getBoard());
                        doMove(move);
                        score += move.getScore();
                    } catch (InvalidMoveException e) {
                        kick(Kick.INVALID_MOVE);
                    }

                } else {
                    kick(Kick.NOT_YOUR_TURN);
                }
                break;
            // client send that it wants to swap
            case Protocol.Client.SWAP:
                if (game.getCurrentClient() == this) {
                    swap(msg);
                    game.nextClient();
                } else {
                    kick(Kick.NOT_YOUR_TURN);
                }
                break;
            default:
                kick(Kick.INVALID_COMMAND);
                break;
        }
        scan.close();
    }

    /**
     * This methode can be used to send a message to the client.
     * On IOException the socket will be closed
     * @param msg String msg is the message that you want to send
     */
    public void sendMessage(String msg) {
        System.out.printf("Server -> %s %s%n", clientName, msg);
        try {
            out.write(msg);
            out.newLine();
            out.flush();
        } catch (IOException e) {
            shutdown();
        }
    }

    /**
     * This ClientHandler signs off from the Server and subsequently
     * sends a last broadcast to the Server to inform that the Client
     * is no longer participating in the chat.
     */
    public void shutdown() {
        System.out.println("Client " + clientName + " has left");
        game.removeHandler(this);
        try {
            out.close();
            in.close();
            sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**.
     * Kick client
     * @param reason String reason, the reason to kick the user
     */
    private void kick(String reason) {
        game.tilesBackToStack(hand);
        game.broadcast(String.format("%s %s %s %s", Protocol.Server.KICK, 
        		  clientNumber, hand.size(), reason));
        shutdown();
    }

    /**.
     * Swap tiles with the stack
     * @param msg the msg which is received from the client
     */
    private void swap(String msg) {
        if (game.getStack().size() == 0) {
            kick(Kick.EMPTY_STACK);
        } else if (Utils.countWords(msg) < 2) {
            kick(Kick.INVALID_COMMAND);
        } else {
            Scanner scan = new Scanner(msg);
            scan.next();
            String line;
            String tileLine = "";
            while (scan.hasNext()) {
                line = scan.next();
                Color c = Color.generateColorFromChar(line.charAt(0));
                Shape s = Shape.generateShapeFromChar(line.charAt(1));
                Tile t = new Tile(c, s);
                // search this tile in the hand
                boolean foundTile = false;
                for (int i = 0; i < hand.size() && !foundTile; i++) {
                    if (hand.get(i).equalsTile(t)) {
                        Tile newTile = game.swap(t);
                        hand.set(i, newTile);
                        // send the tile to client
                        tileLine += newTile + " ";
                        foundTile = true;
                    }
                }
            }
            sendMessage(Protocol.Server.NEW + " " + tileLine);
            game.broadcast(String.format("%s %d empty", Protocol.Server.TURN, clientNumber));
            scan.close();
        }
    }

    /**.
     * Check a received move
     * @param move the PlaceMove of the received move
     * @return the boolean if it's a valid move or not
     * @throws InvalidMoveException
     */
    public boolean check(PlaceMove move) throws InvalidMoveException {
        Board b = game.getBoard();
        List<MoveWrapper> moves = move.getMoves();
        for (MoveWrapper wc : moves) {
            Tile  tile = wc.getTile();
            if (!b.placeTile(tile, wc.getCoordinates().get(0), wc.getCoordinates().get(1), b)) {
                throw new InvalidMoveException(tile, 
                		  wc.getCoordinates().get(0), wc.getCoordinates().get(1));
            }
        }
        for (MoveWrapper wc : moves) {
            Tile  tile = wc.getTile();
            game.getBoard().placeTile(tile, wc.getCoordinates().get(0), 
            		  wc.getCoordinates().get(1), game.getBoard());
            removeFromHand(tile);

        }
        return true;
    }

    /**.
     * Remove the tile from the hand
     * @param tile the tile you want to remove
     */
    private void removeFromHand(Tile tile) {
        boolean removed = false;
        Tile t = null;
        for (Tile handTile : hand) {
            if (!removed && handTile.equalsTile(tile)) {
                t = handTile;
                removed = true;
            }
        }
        if(t != null) {
            hand.remove(t);
        }
    }

    /**.
     * Check is user is able to do a move
     * @return boolean if the user is able to do a move
     */
    public boolean canDoMove() {
        if (game.getStack().size() == 0) {
            List<Integer[]> emptyFields = game.getBoard().emptyFields();
            for (Tile t : hand) {
                for (Integer[] coord : emptyFields) {
                    if (game.getBoard().isValidMove(t, coord[0], coord[1])) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;

    }

    // GETTERS SETTERS

    public int getClientNumber() {
        return clientNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public ArrayList<Tile> getHand() {
        return hand;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setClientNumber(int clientNumber) {
        this.clientNumber = clientNumber;
    }

    public void addToHand(Tile t) {
        hand.add(t);
    }

    public void doMove(PlaceMove move, Boolean first) {
        try {
            check(move);
            // Give new tiles
            String newString = "";
            if (game.getStack().size() != 0) {
                int newTiles = (Utils.countWords(move.toString()) - 1) / 3;
                newTiles = newTiles > game.getStack().size() ? game.getStack().size() : newTiles;
                for (int i = 0; i < newTiles; i++) {
                    Tile t = game.getTileFromStack();
                    addToHand(t);
                    newString += t.toString() + " ";
                }
            } else {
                newString = "empty";
            }
            // only send new tiles if it's not the first move (first move is done by server before sending tiles)
            if (!first) {
                sendMessage(String.format("%s %s", Protocol.Server.NEW, newString));
            }
            game.broadcast(String.format("%s %s %s", Protocol.Server.TURN, 
            		  clientNumber, move.toString().substring(5)));
            System.out.println("stacksize " + game.getStack().size());
            if (!first) {
                game.nextClient();
            }
        } catch (InvalidMoveException e) {
            e.printStackTrace();
            kick(Kick.INVALID_MOVE);
        }
    }

    public void doMove(PlaceMove move) {
        doMove(move, false);

    }

    public int getScore() {
        return score;
    }

    public void setScore(int s) {
        score = s;
    }

    public void connectionLost(){
        // kick the player for connection lost
        if(!game.isEnd()){
            kick(Kick.CONNECTION_LOST);
            if(game.getCurrentClientNumber() == clientNumber){
                game.nextClient();
            }
        }
    }
}