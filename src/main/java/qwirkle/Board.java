package qwirkle;

import qwirkle.ai.Ai;
import qwirkle.ai.Naive;
import qwirkle.exception.InvalidMoveException;
import qwirkle.lib.GameRules;
import qwirkle.move.PlaceMove;
import qwirkle.move.Turn;
import qwirkle.tiles.Color;
import qwirkle.tiles.Shape;
import qwirkle.tiles.Tile;
import qwirkle.util.MoveWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martijn on 11-1-16.
 */
public class Board {

    public static final Tile NONE_TILE = new Tile(Color.NONE, Shape.NONE);

    public PlaceMove move = new PlaceMove(this);

    private Game game;
    private Board deepCopy;
    private int stackSize;
    private Tile[][] matrix;

    /**.
     * Creates a new {@link qwirkle.Board} object
     * Sets the {@link qwirkle.Board#deepCopy} if createDeepCopy is true.
     * @param createDeepCopy
     *      Boolean whether or not a deepcopy should be created
     * @param game
     *      {@link qwirkle.Game} object.
     */
    public Board(Boolean createDeepCopy, Game game) {
        this.game = game;
        matrix = new Tile[GameRules.BOARD_COLS][GameRules.BOARD_ROWS];
        setEmptyMatrix();
        if (createDeepCopy) {
            deepCopy = deepCopy();
        }
    }

    /**.
     * Creates a new {@link qwirkle.Board} object
     * Calls {@link qwirkle.Board#Board(Boolean, Game)} with true as boolean value
     * @see qwirkle.Board#Board(Boolean, Game)
     */
    public Board() {
        this(true, null);
    }

    /**.
     * Creates a new {@link qwirkle.Board} object
     * Calls {@link qwirkle.Board#Board(Boolean, Game)} 
     * with true as boolean value, and the game paramter as second value.
     * @see qwirkle.Board#Board(Boolean, Game)
     * @param game
     *      {@link qwirkle.Game} object to create {@link qwirkle.Board} with.
     */
    public Board(Game game) {
        this(true, game);
        this.game = game;

    }

    /**.
     * Sets the matrix to a matrix full of {@link qwirkle.Board#NONE_TILE}
     */
    private void setEmptyMatrix() {
        for (int i = 0; i < GameRules.BOARD_COLS; i++) {
            for (int j = 0; j < GameRules.BOARD_ROWS; j++) {
                matrix[i][j] = NONE_TILE;
            }
        }
    }

    /**.
     * Tries to place a tile on a selected position.
     * Checks if the move is valid first.
     * @param tile
     *      {@link qwirkle.tiles.Tile} which you want to place.
     * @param row
     *      Int row you want to place in.
     * @param col
     *      Int col you want to place in.
     * @param b
     *      {@link qwirkle.Board} object you want to place on.
     * @return Boolean true if move is place, false otherwise.
     */
    public boolean placeTile(Tile tile, int row, int col, Board b) {
        if (isValidMove(tile, row, col)) {
            b.setField(tile, row, col);
            return true;
        } else {
			return false;
		}
    }

    /**.
     * Sets the field.
     * Should not be used directly.
     * Only usable by deepcopy()
     * @param tile
     *      {@link qwirkle.tiles.Tile} object you want to set
     * @param row
     *      Int row you want to set
     * @param col
     *      Int col you want to set
     *
     */
    private void setField(Tile tile, int row, int col) {
        matrix[row][col] = tile;
    }

    public Board getDeepCopy() {
        return deepCopy;
    }

    /**.
     * returns the {@link qwirkle.tiles.Tile} at {@link qwirkle.Board#matrix}[int][int]
     * @param row
     *      Int row you want to know the tile on
     * @param col
     *      Int col you want to know the tile on
     * @return {@link qwirkle.tiles.Tile} object
     */
    public Tile getField(int row, int col) {
        return matrix[row][col];
    }

    /**.
     * Creates a deepcopy of the board.
     * Sets the matrix with tiles on this board.
     * @return {@link qwirkle.Board#deepCopy} deepcopy
     */
    public Board deepCopy() {
        Board b = new Board(false, this.game);
        for (int i = 0; i < GameRules.BOARD_COLS; i++) {
            for (int j = 0; j < GameRules.BOARD_ROWS; j++) {
                b.setField(getField(i, j), i, j);
            }
        }
        this.deepCopy = b;
        return b;
    }

    /**.
     * Returns the {@link qwirkle.Game} object.
     * @return {@link qwirkle.Game} object.
     */
    public Game getGame() {
        return game;
    }

    /**.
     * Adds a tile to the board.
     * Should be called by the gui
     * @param tile
     *      {@link qwirkle.tiles.Tile} which you want to place
     * @param row
     *      Int row you want to place in
     * @param col
     *      Int col you want to place in
     * @return Boolean true if move is executed, 
     * 		otherwise throws {@link qwirkle.exception.InvalidMoveException}
     * @throws InvalidMoveException If move is not accepted.
     */
    public boolean addTile(Tile tile, int row, int col) throws InvalidMoveException {
        System.out.println(tile.toString() + row + col);
        if (game.getHumanPlayer().hasTile(tile) 
        		  && game.getBoard().deepCopy.placeTile(tile, row, col, deepCopy)) {
            game.getHumanPlayer().removeTileFromHand(tile);
            move.addPlaceMove(tile, row, col);
            return true;
        } else {
			throw new InvalidMoveException(tile, row, col);
		}
    }

    /**
     * Confirms the move.
     * Checks if the move is not empty, and then calls {@link PlaceMove#confirmMyMove()}
     * After that resets the deepcopy, and makes a new placemove.
     * @return Boolean if move is done or not.
     */
    public boolean confirmMove() {
        if (!move.isEmpty()) {
			try {
                move.confirmMyMove();
                deepCopy = deepCopy();
                move = new PlaceMove(this);
                return true;
            } catch (InvalidMoveException e) {
                e.printStackTrace();
            }
		}
        return false;
    }


    /**
     * Cancels a move.
     * Resets the {@link qwirkle.Board#move} and gives the
     *      {@link qwirkle.player.Player} his {@link qwirkle.tiles.Tile}'s back.
     * @return {@link qwirkle.move.PlaceMove} object
     */
    public PlaceMove cancelMove() {
        PlaceMove dupl = move;
        for (MoveWrapper wc : move.getMoves()) {
            Tile tile = wc.getTile();
            game.getHumanPlayer().addTiles(tile);
        }
        move.emptyMove();
        deepCopy = deepCopy();
        return dupl;
    }

    /**
     * Checks if a move is valid.
     * Checks if the first move is at 91, 91 &&
     * checks if the move is connected (except first move) &&
     * if the row is place on a {@link qwirkle.Board#NONE_TILE}
     * @param tile
     *      {@link qwirkle.tiles.Tile} You want to place
     * @param row
     *      Int row you want to place in
     * @param col
     *      Int col you want to place in.
     * @return Boolean true if is valid move, otherwise false.
     */
    public boolean isValidMove(Tile tile, int row, int col) {
        boolean ans = true;
        if (!getField(row, col).toString().equals(NONE_TILE.toString())) {
			return false;
		}
        if (row == ((GameRules.BOARD_COLS / 2) - 1) && col == ((GameRules.BOARD_ROWS / 2) - 1)) {
			return true;
		}
        if (isEmpty(row - 1, col) && isEmpty(row + 1, col) && 
        		  isEmpty(row, col - 1) && isEmpty(row, col + 1)) {
			return false;
		}
        if (!isEmpty(row + 1, col) || !isEmpty(row - 1, col)) {
			ans = checkRow(tile, row, col, false);
		}
        if (ans && (!isEmpty(row, col + 1) || !isEmpty(row, col - 1))) {
			ans = checkRow(tile, row, col, true);
		}
        return ans;
    }


    /**
     * Checks if a move can be placed, according  to what lies in the row.
     * So if the row contains all same colors, and all different shapes, and other way around.
     * @param tile
     *      {@link qwirkle.tiles.Tile} you want to place
     * @param rowArg
     *      Int row where you want to place
     * @param colArg
     *      Int col where you want to place
     * @param horizontalCheck
     *      Boolean if the check should be executed horizontally.
     * @return
     */
    private boolean checkRow(Tile tile, int rowArg, int colArg, boolean horizontalCheck) {
        int row = rowArg;
        int col = colArg;
        int dRow = horizontalCheck ? 0 : 1; //als horizontal check, dan dx 1 dy 0, anders dx 0 dy 1.
        int dCol = horizontalCheck ? 1 : 0;
        int dupCol = col;
        int dupRow = row;

        List<Tile> set = new ArrayList<Tile>();
        set.add(tile);
        while (!isEmpty(row + dRow, col + dCol)) {
            set.add(getField(row + dRow, col + dCol));
            row += dRow;
            col += dCol;
        }
        row = dupRow;
        col = dupCol;
        while (!isEmpty(row - dRow, col - dCol)) {
            set.add(getField(row - dRow, col - dCol));
            row -= dRow;
            col -= dCol;
        }

        if (set.size() > 6) {
			return false;
		}
        if (set.size() <= 1) {
			return true;
		}
        boolean ans = true;
        if (set.get(0).getColor().equals(set.get(1).getColor())) {
            for (int i = 1; i < set.size(); i++) {
                if (!set.get(0).getColor().equals(set.get(i).getColor())
                		  || set.get(0).getShape().equals(set.get(i).getShape())) {
                    ans = false;
                }
            }
        } else if (set.get(0).getShape().equals(set.get(1).getShape())) {
            for (int i = 1; i < set.size(); i++) {
                if (!set.get(0).getShape().equals(set.get(i).getShape()) 
                		  || set.get(0).getColor().equals(set.get(i).getColor())) {
                    ans = false;
                }
            }
        } else {
            ans = false;
        }
        return ans;
    }

    /**.
     * Checks if the tile at {@link qwirkle.Board#getField(int, int)} 
     * 		== {@link qwirkle.Board#NONE_TILE}
     * @param row
     *      Int column to check
     * @param col
     *      Int row to check
     * @return Boolean true if field is empty, otherwise false
     */
    public boolean isEmpty(int row, int col) {
        return getField(row, col).toString().equals(NONE_TILE.toString());
    }

    public List<Integer[]> emptyFields() {
        int[] boundaries = getBoundaries();
        int top = boundaries[0] == 0 ? 0 :  boundaries[0] - 1;
        int bottom = boundaries[1] == GameRules.BOARD_ROWS ? 
        		  GameRules.BOARD_ROWS :  boundaries[1] + 1;
        int left = boundaries[2] == 0 ? 0 : boundaries[2] - 1;
        int right = boundaries[3] == GameRules.BOARD_COLS ? 
        		  GameRules.BOARD_COLS : boundaries[3] + 1;
        List<Integer[]> result = new ArrayList<Integer[]>();

        for (int row = top; row < bottom; row++) {
            for (int col = left; col < right; col++) {
                if (isEmpty(row, col)) {
                    Integer[] coord = new Integer[2];
                    coord[0] = row;
                    coord[1] = col;
                    result.add(coord);
                }
            }
        }
        return result;
    }

    /**.
     * Get the boundaries of the board
     * @return
     *      An int array with top, bottom, left, right
     */
    public int[] getBoundaries() {
        int top = -1;
        int bottom = -1;
        int left = GameRules.BOARD_COLS;
        int right = 0;
        boolean foundOne = false;
        boolean stop = false;


        for (int row = 0; row < matrix.length && !stop; row++) {
            boolean gotOneInRow = false;
            for (int col = 0; col < matrix[row].length; col++) {
                if (!isEmpty(row, col)) {
                    gotOneInRow = true;
                    foundOne = true;
                    if (top == -1) {
                        top = row;
                    }
                    if (col < left) {
                        left = col;
                    }
                    if (col > right) {
                        right = col;
                    }
                    if (row > bottom) {
                        bottom = row;
                    }
                }
            }
            if (!gotOneInRow && foundOne) {
                stop = true;
            }
        }

        int[] result = new int[4];
        result[0] = top == -1 ? 91 : top;
        result[1] = bottom == -1 ? 91 : bottom;
        result[2] = left == GameRules.BOARD_COLS ? 91: left;
        result[3] = right == 0 ? 91 : right;
        return result;
    }

    public void decreaseStackSize(int size) {
        stackSize = stackSize - size;
    }

    public void setStackSize(int stack) {
        this.stackSize = stack;
    }

    public int getStackSize() {
        return stackSize;
    }

    public Turn.Move hint() {
        Ai ai = new Naive(this, game.getLocalPlayer().getHand());
        return ai.determineMove(true);
    }
}
