package qwirkle.move;

import qwirkle.Board;
import qwirkle.exception.InvalidMoveException;

/**
 * Created by martijn on 11-1-16.
 */
public interface Turn {

	/**
	 * . Empties the move
	 */
	void emptyMove();

	/**
	 * . Confirms the move
	 * 
	 * @return Boolean true if succeeded, otherwise throws invalidMoveException
	 * @throws InvalidMoveException
	 *             Thrown when move is not valid.
	 */
	boolean confirmMyMove() throws InvalidMoveException;

	abstract class Move implements Turn {

		protected Board board;

		/**
		 * Sends the move to the server.
		 * 
		 * @param move
		 *            String move string.
		 */
		public void sendMoveToServer(String move) {
			if (board != null) {
				System.out.println(move);
				System.out.println(board);
				board.getGame().getClientConnector().sendToServer(move);
			}
		}
	}
}
