package qwirkle.move;

import qwirkle.Board;
import qwirkle.exception.InvalidMoveException;
import qwirkle.lib.Protocol;
import qwirkle.tiles.Color;
import qwirkle.tiles.Shape;
import qwirkle.tiles.Tile;
import qwirkle.util.MoveWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by martijn on 11-1-16.
 */
public class PlaceMove extends Turn.Move {

	private List<MoveWrapper> moves;

	/**
	 * . Sets the {@link qwirkle.move.PlaceMove#moves} and sets the
	 * {@link qwirkle.move.PlaceMove#board} to boardArg
	 * 
	 * @param boardArg
	 *            {@link qwirkle.Board} object
	 */
	public PlaceMove(Board boardArg) {
		board = boardArg;
		moves = new ArrayList<MoveWrapper>();
	}

	/**
	 * Returns if moves is empty.
	 * 
	 * @return Boolean true if {@link qwirkle.move.PlaceMove#moves} has size 0
	 *         otherwise false.
	 */
	public boolean isEmpty() {
		return moves.isEmpty();
	}

	/**
	 * Adds a placemove.
	 * 
	 * @param tile
	 *            {@link qwirkle.tiles.Tile} you want to place
	 * @param row
	 *            Int row you want to place tile on
	 * @param col
	 *            Int col you want to place tile on
	 */
	public void addPlaceMove(Tile tile, int row, int col) throws InvalidMoveException {
		if (moves.size() > 0) {
			if (isOneLine(tile, row, col)) {
				List<Integer> list = new ArrayList<Integer>();
				list.add(row);
				list.add(col);
				MoveWrapper wc = new MoveWrapper();
				wc.setTile(tile);
				wc.setCoordinates(list);
				moves.add(wc);
			} else {
				throw new InvalidMoveException(tile, row, col);
			}
		} else {
			List<Integer> list = new ArrayList<Integer>();
			list.add(row);
			list.add(col);
			MoveWrapper wc = new MoveWrapper();
			wc.setTile(tile);
			wc.setCoordinates(list);
			moves.add(wc);
		}
	}

	/**
	 * Generates a move from a string.
	 * 
	 * @param moveString
	 *            String you want to extract move from
	 * @param board
	 *            {@link qwirkle.Board} object you want to place on
	 * @return {@link qwirkle.move.PlaceMove} object
	 */
	public static PlaceMove generateMoveFromString(String moveString, Board board) 
			throws InvalidMoveException {
		PlaceMove move = new PlaceMove(board);
		Scanner scanner = new Scanner(moveString);
		boolean isEndOfString = false;
		while (!isEndOfString) {
			String tileIdentifier = scanner.next();
			Color c = Color.generateColorFromChar(tileIdentifier.charAt(0));
			Shape s = Shape.generateShapeFromChar(tileIdentifier.charAt(1));
			Tile t = new Tile(c, s);
			int row = scanner.nextInt();
			int col = scanner.nextInt();
			move.addPlaceMove(t, row, col);
			isEndOfString = !scanner.hasNext();
		}
		scanner.close();
		return move;
	}

	@Override
	public void emptyMove() {
		moves = new ArrayList<MoveWrapper>();
	}

	@Override
	public boolean confirmMyMove() throws InvalidMoveException {
		for (MoveWrapper wc : moves) {
			Tile tile = wc.getTile();
			board.placeTile(tile, wc.getCoordinates().get(0), wc.getCoordinates().get(1), board);
		}
		sendMoveToServer(this.toString());

		board = null;
		return true;
	}

	/**
	 * Executes the move of someone else on the board.
	 * 
	 * @param playerNumber
	 *            Int player number
	 */
	public void doOthersMove(int playerNumber) {
		for (MoveWrapper wc : moves) {
			Tile tile = wc.getTile();
			board.placeTile(tile, wc.getCoordinates().get(0), wc.getCoordinates().get(1), board);
		}
		board.getGame().getPlayer(playerNumber).addScore(getScore());
	}

	/**.
	 * Returns the score for a move
	 * 
	 * @return Int score
	 */
	public int getScore() {
		int score = 0;
		int sScore = 0;
		boolean firstFound = false;
		if (moves.size() > 1) {
			boolean horizonal = moves.get(0).getCoordinates().get(0) == 
					  moves.get(1).getCoordinates().get(0);
			for (int i = 0; i < moves.size(); i++) {
				int dRow = horizonal ? 1 : 0;
				int dCol = dRow == 1 ? 0 : 1;
				int row = moves.get(i).getCoordinates().get(0);
				int col = moves.get(i).getCoordinates().get(1);
				int dupRow = row;
				int dupCol = col;
				sScore = 0;
				while (!board.isEmpty(row + dRow, col + dCol)) {
					if (!firstFound) {
						sScore++;
						firstFound = true;
					}
					sScore++;
					row += dRow;
					col += dCol;
				}
				row = dupRow;
				col = dupCol;
				while (!board.isEmpty(row - dRow, col - dCol)) {
					if (!firstFound) {
						sScore++;
						firstFound = true;
					}
					sScore++;
					row -= dRow;
					col -= dCol;
				}
				sScore = sScore == 6 ? 12 : sScore;
				score = score + sScore;
			}
			firstFound = false;
			int dRow = horizonal ? 0 : 1;
			int dCol = dRow == 1 ? 0 : 1;
			int row = moves.get(0).getCoordinates().get(0);
			int col = moves.get(0).getCoordinates().get(1);
			int dupRow = row;
			int dupCol = col;
			while (!board.isEmpty(row + dRow, col + dCol)) {
				if (!firstFound) {
					sScore++;
					firstFound = true;
				}
				score++;
				row += dRow;
				col += dCol;
			}
			row = dupRow;
			col = dupCol;
			while (!board.isEmpty(row - dRow, col - dCol)) {
				if (!firstFound) {
					sScore++;
					firstFound = true;
				}
				score++;
				row -= dRow;
				col -= dCol;
			}
			score = score + sScore;
		} else { // een aanleggen
			score = 0;
			int dRow = 1;
			int dCol = 0;
			int row = moves.get(0).getCoordinates().get(0);
			int col = moves.get(0).getCoordinates().get(1);
			int dupRow = row;
			int dupCol = col;
			boolean found = false;
			while (!board.isEmpty(row + dRow, col + dCol)) {
				if (!found) {
					found = true;
					score++;
				}
				score++;
				row += dRow;
				col += dCol;
			}
			score = score == 6 ? score * 2 : score;
			row = dupRow;
			col = dupCol;
			while (!board.isEmpty(row - dRow, col - dCol)) {
				if (!found) {
					found = true;
					score++;
				}
				score++;
				row -= dRow;
				col -= dCol;
			}
			found = false;
			dRow = 0;
			dCol = 1;
			row = dupRow;
			col = dupCol;
			while (!board.isEmpty(row + dRow, col + dCol)) {
				if (!found) {
					found = true;
					score++;
				}
				score++;
				row += dRow;
				col += dCol;
			}
			row = dupRow;
			col = dupCol;
			while (!board.isEmpty(row - dRow, col - dCol)) {
				if (!found) {
					found = true;
					score++;
				}
				score++;
				row -= dRow;
				col -= dCol;
			}
			score = score == 6 ? score * 2 : score;
		}
		return score;
	}

	@Override
	public String toString() {
		String ans = Protocol.Client.MOVE;
		for (MoveWrapper wc : moves) {
			Tile tile = wc.getTile();
			ans = ans + " " + tile + " " + wc.getCoordinates().get(0) 
					+ " " + wc.getCoordinates().get(1);
		}
		return ans;
	}

	/**.
	 * Returns all moves in the object
	 * 
	 * @return List moves.
	 */
	public List<MoveWrapper> getMoves() {
		return moves;
	}

	/**.
	 * Helper function for checking if everything in a move is in one line
	 * 
	 * @param tile
	 *            {@link qwirkle.tiles.Tile} you want to place
	 * @param row
	 *            Int row where you want to place
	 * @param col
	 *            Int col where you want to place
	 * @return Boolean if everything is in one line, otherwise false
	 */
	private boolean isOneLine(Tile tile, int row, int col) {
		List<MoveWrapper> wcc = new ArrayList<MoveWrapper>();
		for (MoveWrapper w : moves) {
			wcc.add(w);
		}
		MoveWrapper wc1 = new MoveWrapper();
		wc1.setTile(tile);
		List<Integer> list = new ArrayList<Integer>();
		list.add(row);
		list.add(col);
		wc1.setCoordinates(list);
		wcc.add(wc1);
		int[] a = new int[wcc.size()];
		int[] b = new int[wcc.size()];
		for (MoveWrapper wc : wcc) {
			boolean done = false;
			for (int i = 0; i < a.length; i++) {
				if (!done) {
					if (a[i] == 0) {
						a[i] = wc.getCoordinates().get(0);
						b[i] = wc.getCoordinates().get(1);
						done = true;
					}
				}
			}
		}
		boolean c = true;
		boolean d = true;
		for (int i = 0; i < a.length - 1; i++) {
			if (!(a[i] == a[i + 1])) {
				c = false;
			}
			if (!(b[i] == b[i + 1])) {
				d = false;
			}
		}
		return c || d;
	}
}
