package qwirkle.move;

import qwirkle.Board;
import qwirkle.exception.InvalidMoveException;
import qwirkle.lib.Protocol;
import qwirkle.tiles.Tile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martijn on 11-1-16.
 */
public class SwitchMove extends Turn.Move {

    private List<Tile> switchTiles;
    private boolean empty;

    /**.
     * Sets the {@link qwirkle.move.SwitchMove#switchTiles}
     * and sets the {@link qwirkle.move.SwitchMove#board} to boardArg
     *
     * @param boardArg {@link qwirkle.Board} object
     */
    public SwitchMove(Board boardArg) {
        switchTiles = new ArrayList<Tile>();
        board = boardArg;
    }

    /**.
     * Adds a switch move to this object
     *
     * @param tile {@link qwirkle.tiles.Tile} object which you want to switch
     */
    public void addSwitchMove(Tile tile) {
        switchTiles.add(tile);
    }

    @Override
    public void emptyMove() {
        switchTiles = new ArrayList<Tile>();
    }

    @Override
    public boolean confirmMyMove() throws InvalidMoveException {
        if (switchTiles.size() > board.getStackSize()) {
            throw new InvalidMoveException("Stack size too low");
        }
        boolean ans = true;
        for (Tile tile : switchTiles) {
            if (!board.getGame().getLocalPlayer().hasTile(tile)) {
                ans = false;
            }
        }
        if (ans) {
            for (Tile tile : switchTiles) {
                board.getGame().getLocalPlayer().removeTileFromHand(tile);
            }
        }
        sendMoveToServer(this.toString());
        return ans;
    }


    @Override
    public String toString() {
        String ans = Protocol.Client.SWAP;
        for (Tile tile : switchTiles) {
            ans = ans + " " + tile;
        }
        return ans;
    }

    public boolean isEmpty() {
        return switchTiles.size() == 0;
    }
}
