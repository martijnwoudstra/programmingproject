package qwirkle.player.local;

import qwirkle.Board;
import qwirkle.ai.Ai;
import qwirkle.ai.Mediocre;
import qwirkle.exception.InvalidMoveException;
import qwirkle.move.PlaceMove;
import qwirkle.move.SwitchMove;
import qwirkle.move.Turn;
import qwirkle.tiles.Tile;

import java.util.ArrayList;
import java.util.concurrent.*;

/**
 * Created by Martijn on 18-Dec-15.
 */
public class ComputerPlayer extends LocalPlayer {

    long AITime = 10000L;
    private Ai ai;
    /**
     * Creates a ComputerPlayer object.
     *
     * @param playerName
     *      String name of the player. GUI
     */
    public ComputerPlayer(String playerName, int playerNumber) {
        hand = new ArrayList<Tile>();
        name = playerName;
        number = playerNumber;
    }

    /**
     * Determines the move of a player.
     * For HumanPlayer it is based on input.
     * For ComputerPlayers it is based on an algorithm.
     *
     * @param board Board object.
     */
    public void determineMove(Board board) {
        ai = new Mediocre(board, hand);
        Turn.Move pm;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<?> future = executor.submit(new Runnable() {
            @Override
            public void run() {
                ai.determineMove(false);
            }
        });

        executor.shutdown();

        try {
            future.get(AITime, TimeUnit.MILLISECONDS);
            pm = ai.getMove();
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            System.out.println("Ai exceed time");
            SwitchMove sw = new SwitchMove(board);
            sw.addSwitchMove(hand.get(0));
            pm = sw;
        }

        try {
            if(pm instanceof PlaceMove){
                PlaceMove m = (PlaceMove)pm;
                m.confirmMyMove();
            } else {
                SwitchMove m = (SwitchMove)pm;
                m.confirmMyMove();
            }
        } catch (InvalidMoveException e) {
            e.printStackTrace();
        }
    }

    public void setAITime(Long time){
        AITime = time;
    }
}
