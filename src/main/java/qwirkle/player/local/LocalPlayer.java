package qwirkle.player.local;

import qwirkle.Board;
import qwirkle.ai.Ai;
import qwirkle.ai.Naive;
import qwirkle.exception.InvalidMoveException;
import qwirkle.move.SwitchMove;
import qwirkle.move.Turn;
import qwirkle.player.Player;
import qwirkle.tiles.Tile;

import java.util.ArrayList;
import java.util.concurrent.*;

/**
 * Created by Martijn on 18-Dec-15.
 */
public abstract class LocalPlayer extends Player {

    protected ArrayList<Tile> hand;

    public int number;


    public abstract void determineMove(Board board);

    /**
     * Adds tiles to hand.
     * Used by the {@link qwirkle.network.ClientConnector} to give new tiles from the server.
     * @param tile
     *      {@link qwirkle.tiles.Tile} you want to add
     */
    public void addTiles(Tile tile) {
        hand.add(tile);
    }

    /**.
     * Returns if the {@link qwirkle.player.Player} has a tile
     * @param tile
     *      {@link qwirkle.tiles.Tile} you want to know
     * @return Boolean true if player has it, otherwise false.
     */
    public boolean hasTile(Tile tile) {
        boolean ans = false;
        for (Tile handTile : hand) {
            if (handTile.toString().equals(tile.toString())) {
                ans = true;
            }
        }
        return ans;
    }

    /**
     * Removes a tile from a {@link qwirkle.player.Player}s hand.
     * @param tile
     *      {@link qwirkle.tiles.Tile} you want to remove
     */
    public void removeTileFromHand(Tile tile) {
        boolean removed = false;
        Tile t = null;
        for (Tile handTile : hand) {
            if (!removed && handTile.equalsTile(tile)) {
                t = handTile;
                removed = true;
            }
        }
        if(removed) {
            hand.remove(t);
        }
    }

    /**.
     * Returns the hand of a {@link qwirkle.player.Player}
     * @return
     *      ArrayList the hand
     */
    public ArrayList<Tile> getHand() {
        return hand;
    }
}
