package qwirkle.player.local;

import qwirkle.Board;
import qwirkle.tiles.Tile;

import java.util.ArrayList;

/**
 * Created by Martijn on 18-Dec-15.
 */
public class HumanPlayer extends LocalPlayer {

    /**
     * Creates a HumanPlayer.
     *
     * @param playerName
     *      String name of the player. Used in GUI
     */
    public HumanPlayer(String playerName, int playerNumber) {
        hand = new ArrayList<Tile>();
        name = playerName;
        number = playerNumber;
    }


    @Override
    @Deprecated
    public void determineMove(Board board) {
    }
}
