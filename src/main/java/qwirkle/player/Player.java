package qwirkle.player;

import java.util.Observable;

/**
 * Created by Martijn on 18-Dec-15.
 */
public abstract class Player extends Observable {

    /**.
     * Name of the player, used for GUI
     */
    public String name;

    public int score;

    /**.
     * Returns the score of the player
     * @return Int score
     */
    public int getScore() {
        return score;
    }

    /**.
     * Adds the scoreArg to the players score
     * @param scoreArg
     *      Int scoreArg to add to player scoreS
     */
    public void addScore(int scoreArg) {
        this.score = this.score + scoreArg;
    }
}
