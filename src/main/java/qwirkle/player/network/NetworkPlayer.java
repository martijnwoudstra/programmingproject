package qwirkle.player.network;

import qwirkle.player.Player;

/**
 * Created by Martijn on 18-Dec-15.
 */
public class NetworkPlayer extends Player {

    /**
     * Creates a network player.
     * Sets the player name, and playernumber
     * @param name
     *      String player name
     */
    public NetworkPlayer(String name) {
        this.name = name;
    }
}
