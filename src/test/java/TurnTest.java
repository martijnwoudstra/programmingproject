import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import qwirkle.Board;
import qwirkle.Game;
import qwirkle.move.PlaceMove;
import qwirkle.network.server.Server;
import qwirkle.tiles.Color;
import qwirkle.tiles.Shape;
import qwirkle.tiles.Tile;

import java.io.IOException;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by martijn on 25-1-16.
 */
public class TurnTest {

    public static Server server;
    public static Game game;
    private static Game game2;

    public static Board board;
    public static final Tile TILE_1 = new Tile(Color.PURPLE, Shape.CIRCLE);
    public static final Tile TILE_2 = new Tile(Color.PURPLE, Shape.SQUARE);
    public static final String MOVE_STRING = "Po 91 91 Ps 91 90 Pd 91 89 P* 91 88 Pc 91 87 Px 91 86";


    @BeforeClass
    public static void doo(){
        server = new Server();
        server.startServer(8745, 1000);
    }
    /**
     * Starts up a game, and creates a board, server, connects, creates human player, and addes tiles.
     */
    @Before
    public void setup() {
        game = new Game();
        game2 = new Game();
        try {
            game.connectToServer("localhost", 8745, "martijn", false);
            game2.connectToServer("localhost", 8745, "me", false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        board = game.getBoard();
        while(game.getLocalPlayer() == null) {}
        System.out.println(game.getLocalPlayer().getHand());
        server.startGame();
    }

    @AfterClass
    public static void after(){
        server.shutdown();
    }

    @Test
    public void testEmpty() throws Exception{
        PlaceMove move = new PlaceMove(board);
        assertTrue(move.isEmpty());
        move.addPlaceMove(TILE_1, 91, 91);
        assertFalse(move.isEmpty());
    }
}
