
import org.junit.Before;
import org.junit.Test;
import qwirkle.network.server.Server;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Created by martijn on 11-1-16.
 */

/**
 * Tests the functionlity of the network.server package
 */
public class ServerTest {

    public Server server;

    @Before
    public void setup() {
        server = new Server();
    }

    @Test
    public void testUnusedPort(){
        assertTrue(server.startServer(1234, 1000));
        server.shutdown();
    }
    @Test
    public void testUsedPort(){
        assertTrue(server.startServer(1234, 1000));
        assertFalse(server.startServer(1234, 1000));
        server.shutdown();
    }







}