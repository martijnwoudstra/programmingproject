import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import qwirkle.Board;
import qwirkle.Game;
import qwirkle.exception.InvalidMoveException;
import qwirkle.lib.GameRules;
import qwirkle.network.server.Server;
import qwirkle.tiles.Color;
import qwirkle.tiles.Shape;
import qwirkle.tiles.Tile;

import java.io.IOException;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by martijn on 11-1-16.
 */

/**
 * Tests the functionalities of Board.java
 * Each test is executed on a copy of a board.
 * This way all tests are indipendent of eachother
 */
public class BoardTest {

    public static Server server;
    public static Game game;
    private static Game game2;

    public static Board board;
    public static final Tile TILE_1 = new Tile(Color.PURPLE, Shape.CIRCLE);
    public static final Tile TILE_2 = new Tile(Color.PURPLE, Shape.SQUARE);
    public static final Tile TILE_3 = new Tile(Color.PURPLE, Shape.DIAMOND);
    public static final Tile TILE_4 = new Tile(Color.PURPLE, Shape.STAR);
    public static final Tile TILE_5 = new Tile(Color.PURPLE, Shape.CLOVER);
    public static final Tile TILE_6 = new Tile(Color.PURPLE, Shape.CROSS);
    public static final Tile TILE_7 = new Tile(Color.RED, Shape.CROSS);


    @BeforeClass
    public static void doo(){
        server = new Server();
        server.startServer(8745, 1000);
    }
    /**
     * Starts up a game, and creates a board, server, connects, creates human player, and addes tiles.
     */
    @Before
    public void setup() {
        game = new Game();
        game2 = new Game();
        try {
            game.connectToServer("localhost", 8745, "martijn", false);
            game2.connectToServer("localhost", 8745, "me", false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        board = game.getBoard();
        while(game.getLocalPlayer() == null) {}
        System.out.println(game.getLocalPlayer().getHand());
        server.startGame();
    }

    @AfterClass
    public static void after(){
        server.shutdown();
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void wrongMatrixCreationLow(){
        board.getField(-1, -1);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void wrongMatrixCreationHigh(){
        board.getField(300, 300);

    }

    @Test
    public void rightMatrix(){
        assertTrue(Board.NONE_TILE.equalsTile(board.getField(0, 0)));
        assertTrue(Board.NONE_TILE.equalsTile(board.getField(GameRules.BOARD_ROWS - 1, GameRules.BOARD_COLS - 1)));
    }


    @Test
    public void testPlaceTile() throws Exception {
        game.getLocalPlayer().addTiles(TILE_1);
        try {
            assertTrue(board.addTile(TILE_1, 91, 91));
        } catch (InvalidMoveException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetField() throws Exception {
        game.getLocalPlayer().addTiles(TILE_1);
        try {
            assertTrue(board.addTile(TILE_1, 91, 91));
        } catch (InvalidMoveException e) {
            e.printStackTrace();
        }
        board.confirmMove();
        assertTrue(TILE_1.equalsTile(board.getField(91, 91)));
    }

    @Test
    public void testConfirmMove() throws Exception {
        testPlaceTile();
        assertTrue(board.confirmMove());
    }

    @Test
    public void testCancelMove() throws Exception {
        testPlaceTile();
        board.cancelMove();
        assertTrue(board.move.getMoves().size() == 0);
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertTrue(board.isEmpty(90, 90));
    }

    @Test
    public void testEmptyFields() throws Exception {
        boolean ans = true;
        List<Integer[]> list = board.emptyFields();
        for(Integer[] ints : list){
            if(!board.isEmpty(ints[0], ints[1]))
                ans = false;
        }
        assertTrue(ans);
    }

    @Test
    public void testDescreaseStackSize() throws Exception {
        board.setStackSize(10);
        board.decreaseStackSize(2);
        assertEquals(board.getStackSize(), 8);
    }

    @Test
    public void testSetStackSize() throws Exception {
        board.setStackSize(10);
        assertEquals(10, board.getStackSize());
    }

    @Test
    public void testGetStackSize() throws Exception {

        board.setStackSize(10);
        assertEquals(10, board.getStackSize());
    }

    @Test
    public void makeMoveMoves(){
        System.out.println(game.getHumanPlayer().getHand());
        game.getLocalPlayer().addTiles(TILE_1);
        game.getLocalPlayer().addTiles(TILE_2);
        game.getLocalPlayer().addTiles(TILE_3);
        game.getLocalPlayer().addTiles(TILE_4);
        game.getLocalPlayer().addTiles(TILE_5);
        game.getLocalPlayer().addTiles(TILE_6);
        try {
            assertTrue(board.addTile(TILE_1, 91, 91));
            assertTrue(board.addTile(TILE_2, 91, 90));
            assertTrue(board.addTile(TILE_3, 91, 89));
            board.addTile(TILE_4, 91, 88);
            board.addTile(TILE_5, 91, 87);
            board.addTile(TILE_6, 91, 86);
        }
        catch (InvalidMoveException e){
            e.printStackTrace();
        }
        assertTrue(board.confirmMove());
    }

    @Test
    public void makeWrongMoveColor(){
        game.getLocalPlayer().addTiles(TILE_1);
        game.getLocalPlayer().addTiles(TILE_2);
        game.getLocalPlayer().addTiles(TILE_3);
        game.getLocalPlayer().addTiles(TILE_4);
        game.getLocalPlayer().addTiles(TILE_5);
        game.getLocalPlayer().addTiles(TILE_7);
        try {
            board.addTile(TILE_1, 91, 91);
            board.addTile(TILE_2, 91, 90);
            board.addTile(TILE_3, 91, 89);
            board.addTile(TILE_4, 91, 88);
            board.addTile(TILE_5, 91, 87);
            assertFalse(board.addTile(TILE_7, 91, 86));
        }
        catch (InvalidMoveException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void makeWrongMoveSeven(){
        game.getLocalPlayer().addTiles(TILE_1);
        game.getLocalPlayer().addTiles(TILE_2);
        game.getLocalPlayer().addTiles(TILE_3);
        game.getLocalPlayer().addTiles(TILE_4);
        game.getLocalPlayer().addTiles(TILE_5);
        game.getLocalPlayer().addTiles(TILE_6);
        game.getLocalPlayer().addTiles(TILE_7);
        try {
            board.addTile(TILE_1, 91, 91);
            board.addTile(TILE_2, 91, 90);
            board.addTile(TILE_3, 91, 89);
            board.addTile(TILE_4, 91, 88);
            board.addTile(TILE_5, 91, 87);
            board.addTile(TILE_6, 91, 86);
            assertFalse(board.addTile(TILE_7, 91, 85));
        } catch (InvalidMoveException e){
            e.printStackTrace();
        }
    }

    @Test(expected = InvalidMoveException.class)
    public void testWrongPlaceTile() throws InvalidMoveException{
        game.getLocalPlayer().addTiles(TILE_1);
        board.addTile(TILE_1, 90, 91);
    }
}