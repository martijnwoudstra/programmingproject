import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import qwirkle.Board;
import qwirkle.Game;
import qwirkle.move.PlaceMove;
import qwirkle.network.server.Server;
import qwirkle.tiles.Color;
import qwirkle.tiles.Shape;
import qwirkle.tiles.Tile;
import qwirkle.util.MoveWrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by martijn on 25-1-16.
 */
public class PlaceMoveTest {

    public static Server server;
    public static Game game;
    private static Game game2;

    public static Board board;
    public static final Tile TILE_1 = new Tile(Color.PURPLE, Shape.CIRCLE);
    public static final Tile TILE_2 = new Tile(Color.PURPLE, Shape.SQUARE);
    public static final Tile TILE_3 = new Tile(Color.PURPLE, Shape.CLOVER);
    public static final Tile TILE_4 = new Tile(Color.PURPLE, Shape.CROSS);
    public static final Tile TILE_5 = new Tile(Color.PURPLE, Shape.DIAMOND);
    public static final Tile TILE_6 = new Tile(Color.PURPLE, Shape.STAR);
    public static final String MOVE_STRING = "Po 91 91 Ps 91 90 Pd 91 89 P* 91 88 Pc 91 87 Px 91 86";


    @BeforeClass
    public static void doo(){
        server = new Server();
        server.startServer(8745, 1000);
    }
    /**
     * Starts up a game, and creates a board, server, connects, creates human player, and addes tiles.
     */
    @Before
    public void setup() {
        game = new Game();
        game2 = new Game();
        try {
            game.connectToServer("localhost", 8745, "martijn", false);
            game2.connectToServer("localhost", 8745, "me", false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        board = game.getBoard();
        while(game.getLocalPlayer() == null || game2.getLocalPlayer() == null) {}
        game.getLocalPlayer().addTiles(TILE_1);
        game.getLocalPlayer().addTiles(TILE_2);
        game.getLocalPlayer().addTiles(TILE_3);
        game.getLocalPlayer().addTiles(TILE_4);
        game.getLocalPlayer().addTiles(TILE_5);
        game.getLocalPlayer().addTiles(TILE_6);
        server.startGame();
    }

    @AfterClass
    public static void after(){
        server.shutdown();
    }


    @Test
    public void testGenerateMoveFromString() throws Exception {
        PlaceMove move = PlaceMove.generateMoveFromString(MOVE_STRING, board);
        assertTrue(move.toString().equals("MOVE " + MOVE_STRING));
        List<MoveWrapper> moves = new ArrayList<MoveWrapper>();

        MoveWrapper mw_1 = new MoveWrapper();
        mw_1.setTile(new Tile(Color.PURPLE, Shape.CIRCLE));
        List<Integer> list1 = new ArrayList<Integer>();
        list1.add(91);
        list1.add(91);
        mw_1.setCoordinates(list1);
        moves.add(mw_1);

        MoveWrapper mw_2 = new MoveWrapper();
        mw_2.setTile(new Tile(Color.PURPLE, Shape.SQUARE));
        List<Integer> list2 = new ArrayList<Integer>();
        list2.add(91);
        list2.add(90);
        mw_2.setCoordinates(list2);
        moves.add(mw_2);

        MoveWrapper mw_3 = new MoveWrapper();
        mw_3.setTile(new Tile(Color.PURPLE, Shape.DIAMOND));
        List<Integer> list3 = new ArrayList<Integer>();
        list3.add(91);
        list3.add(89);
        mw_3.setCoordinates(list3);
        moves.add(mw_3);

        MoveWrapper mw_4 = new MoveWrapper();
        mw_4.setTile(new Tile(Color.PURPLE, Shape.STAR));
        List<Integer> list4 = new ArrayList<Integer>();
        list4.add(91);
        list4.add(88);
        mw_4.setCoordinates(list4);
        moves.add(mw_4);

        MoveWrapper mw_5 = new MoveWrapper();
        mw_5.setTile(new Tile(Color.PURPLE, Shape.CLOVER));
        List<Integer> list5 = new ArrayList<Integer>();
        list5.add(91);
        list5.add(87);
        mw_5.setCoordinates(list5);
        moves.add(mw_5);

        MoveWrapper mw_6 = new MoveWrapper();
        mw_6.setTile(new Tile(Color.PURPLE, Shape.CROSS));
        List<Integer> list6 = new ArrayList<Integer>();
        list6.add(91);
        list6.add(86);
        mw_6.setCoordinates(list6);
        moves.add(mw_6);

        String moveS = "MOVE";
        for (MoveWrapper mws : moves)
            moveS += mws.toString();
        assertTrue(move.toString().equals(moveS));
    }

    @Test
    public void testAddPlaceMove() throws Exception {
        PlaceMove move = new PlaceMove(board);
        move.addPlaceMove(TILE_1, 91, 91);
        assertTrue(move.toString().equals("MOVE " + TILE_1.toString() + " " + 91 + " " + 91));
        assertTrue(move.confirmMyMove());
        assertTrue(board.getField(91, 91).equalsTile(TILE_1));
    }
}
