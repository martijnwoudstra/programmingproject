import org.junit.Before;
import org.junit.Test;
import qwirkle.Game;
import qwirkle.network.server.Server;
import qwirkle.network.server.ServerGame;
import qwirkle.tiles.Color;
import qwirkle.tiles.Shape;
import qwirkle.tiles.Tile;

import java.io.IOException;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Created by martijn on 11-1-16.
 */

/**
 * Tests the functionlity of the network.server package
 */
public class ServerGameTest {

    public Server server;
    public List<ServerGame> games;
    Game game1 = new Game();
    Game game2 = new Game();

    @Before
    public void setup() {
        server = new Server();
        server.startServer(1234, 1000);
        try {
            game1.connectToServer("localhost", 1234, "gnull", false);
            game2.connectToServer("localhost", 1234, "gone", false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(game1.getLocalPlayer() == null || game2.getLocalPlayer() == null){}
        games = server.getGames();
    }

    @Test
    public void testGameSize(){

        assertTrue(games.get(0).getThreads().size() == 2);
        assertTrue(server.startGame());
        server.shutdown();
    }

    @Test
    public void testTileFromStack(){
        int size = games.get(0).getStack().size();
        games.get(0).getTileFromStack();
        assertTrue(games.get(0).getStack().size() == size -1);
        server.shutdown();
    }

    @Test
    public void testSwap(){
        int size = games.get(0).getStack().size();
        Tile t = new Tile(Color.BLUE, Shape.CIRCLE);
        games.get(0).swap(t);
        assertTrue(games.get(0).getStack().size() == size);
        server.shutdown();
    }


    






}